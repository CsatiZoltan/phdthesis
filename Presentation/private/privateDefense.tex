% To be used with the "mypresentation.sty" template

% !TEX root = privateDefense.tex

% The next four lines are directives for the arara TeX automation tool
% arara: pdflatex
% arara: biber
% arara: pdflatex
% arara: pdflatex: {synctex: yes}

\documentclass[aspectratio=43]{beamer} % our projector has 4:3 aspect ratio; for widescreen use 169

\usepackage{../mypresentation} % globally applicable packages, styles and definitions
\usepackage{tikz}

\usetikzlibrary{arrows.meta} % customize arrows
\tikzstyle{every picture}+=[remember picture] % allows referencing nodes outside the current picture
\tikzstyle{top} = [baseline=-1ex] % used for node position at the top of the current line height
\tikzstyle{middle} = [baseline=-0.5ex]

\bibliography{../../Bibliography/FEM,../../Bibliography/XFEM,../../Bibliography/CompGeom,../../Bibliography/Crack,../../Bibliography/SaddlePoint}
\graphicspath{{Pictures/}}

% Definitions
\input{../definitions}

% Title page
\title[Mesh-independent modelling of diffuse cracking]{Mesh-independent modelling of diffuse cracking in cohesive grain-based materials \\[1em] {\small Private defense}}
\author{Zoltan Csati}
\date{
   Brussels \\
   25th September 2019 \\[2em]
   \begin{changemargin}{0cm}{0cm}
      \includegraphics[scale=0.06]{logoECN.pdf} \hfill \includegraphics[scale=0.07]{logoGeM.pdf} \hfill  \includegraphics[scale=0.25]{logoULB_noabbrv.png} \hfill \includegraphics[scale=0.055]{logoBATir.pdf}
   \end{changemargin}
}



\begin{document}

\insertTitlePage

\insertTOC

% Main body of the presentation
\section[Overview]{Overview of the thesis}
\begin{frame}
   \insertSectionTitle
   \begin{changemargin}{-1cm}{-2cm}
      \begin{minipage}[b]{0.6\textwidth}
         \begin{itemize}
            \item Aim
            \begin{itemize}
               \item multiple cracking in cohesive grain-based materials
               \item offer a computational tool for microstructural simulations
               \item capture the relevant physics but be computationally efficient
               \item no user intervention
            \end{itemize}
         \end{itemize}
      \end{minipage}
      \begin{minipage}[b]{0.35\textwidth}
         \tikz \coordinate (n1) at (1cm,-1.82cm);
         \includegraphics[scale=0.3]{granite}
      \end{minipage} 
      \begin{itemize}
         \item<2-> Modelling assumptions
         \begin{itemize}
            \item modelling at the microstructural (grain) level
            \item linear elastic grains
            \item crack propagation along grain boundaries $\rightarrow$ a-priori known crack paths
                  \tikz[top] \coordinate (n2) at (-2.5cm,0);
            \item<4-> extrinsic cohesive zone model (no need for penalty stiffness)
            \item<4-> Mohr-Coulomb failure criterion with tension cut-off and compression cap
            \item<4-> non-uniform and compression-dependent fracture energy: $\GII = f(t_n)$
            \item<4-> 2D
         \end{itemize}
      \end{itemize}
   \end{changemargin}
   \only<3->{
   \begin{tikzpicture}[overlay]
      \path[-{>[scale=1.5]}, line width=2pt, color=red]<2-> (n2) edge [bend left, out=0] (n1);
   \end{tikzpicture}
   }
\end{frame}



\section{Contributions}

\subsection[Stable discretization]{Stable discretization for CutFEM with Lagrange multipliers}

\begin{frame}
   \insertSubsectionTitle
   \begin{itemize}
      \item Two-field formulation
      \item CutFEM discretization
      \item Reduction algorithm for Lagrange multipliers -- \alert{extending \customfootcite{Bechet2009}}
      \begin{center}
         \includegraphics[scale=0.65]{LagMultTying_junction.pdf} \hfill \includegraphics[width=0.28\linewidth]{LagMultShapeFcn_junction.tikz}
      \end{center}
      \item Checking stability for non-conforming discretization
%      \begin{itemize}
%         \item boundedness of $a(u_h,v_h)$: follows from the use of appropriate norms \customfootcite{Boffi2013}
%         \item inf-sup condition on $b(u_h,\lambda_h)$: characteristic eigenvalue problem \customfootcite{El-Abbasi2001}
%         \item inf-sup condition on $a(u_h,v_h)$: characteristic eigenvalue problem \customfootcite{Arnold2009}
%      \end{itemize}
   \end{itemize}
\end{frame}


\subsection{Arbitrary crack configurations}

\begin{frame}
   \insertSubsectionTitle
   \begin{changemargin}{-1cm}{-2cm}
      \begin{minipage}[b]{0.75\textwidth}
         \begin{itemize}
            \item The cracks can arbitrarily cut the background mesh
            \item Multiple cracks can intersect (even partially) an element
            \item Polygon-polygon intersection \customfootcite{Howard2009}
         \end{itemize}
      \end{minipage}
      \begin{minipage}[b]{0.2\textwidth}
         \includegraphics[scale=0.08]{arbitraryCutting.png}
      \end{minipage}
      \begin{itemize}
         \item Constrained Voronoi triangulation of the intersection
         \item Subdivision performed on the \emph{physical} element \customfootcite{Sabada2015}
         \item \alert{CutFEM on Q1 elements with Lagrange multipliers}
      \end{itemize}
   \end{changemargin}
\end{frame}


\subsection{Explicit damage model}

\begin{frame}
   \insertSubsectionTitle
   \begin{changemargin}{-1cm}{-2cm}
      \begin{itemize}
         \item Cohesive tractions in compliance form \customfootcite{Cazes2012}
            \small
            \begin{equation*}
      	      \begin{gathered}
         	      \int\limits_{\Omega}\bm{\varepsilon}(\mathbf{v}):\bm{\mathcal{C}}:\bm{\varepsilon}(\mathbf{u}) \DIFF \Omega + \int\limits_{\Gamma} \jump{\mathbf{v}}\cdot \mleft( \bm{\zeta}-\mathbf{k}\cdot\jump{\mathbf{u}} \mright) \DIFF \Gamma = \int\limits_{\Gamma_N} \mathbf{v}\cdot\mathbf{t}_N \DIFF \Gamma - \int\limits_{\Gamma_D} \mathbf{v}\cdot\mathbf{k}\cdot\mathbf{u}_D \DIFF \Gamma, \\
         	      \int\limits_{\Gamma} \bm{\eta}\cdot \mleft( \jump{\mathbf{u}} - \chi d \mathbf{k}^{-1}\cdot\bm{\zeta} \mright) \DIFF \Gamma = \int\limits_{\Gamma_D} \bm{\eta}\cdot\mathbf{u}_D \DIFF \Gamma.
      	      \end{gathered}
           	\end{equation*}
         \normalsize
         \item Avoiding self-contact, \alert{enforced in an explicit way}
         \item Reformulation of the CZM with an internal variable \customfootcite{Le2018}
         \item \alert{Uncoupled normal and tangential components of the energetic quantities}
         \item \alert{Computation based on energy dissipation}
         \begin{itemize}
            \item pure mode I: cohesive zone becomes independent of $k$
            \item mixed mode: ensures proper energy release in the explicit algorithm
            \item lower bound on $k$, based on the failure criterion
         \end{itemize}
      \end{itemize}
   \end{changemargin}
\end{frame}


\subsection[Implementation]{Careful implementation}

\begin{frame}
   \insertSubsectionTitle
   \begin{itemize}
      \item Robustness for any cutting scenarios
      \item Mildly decreased ill-conditioning
      \item Parallel local computations (cutting, assembly)
      \item Several \emph{local} background meshes, one (vectorial) DOF per node
      \item Damage interpolated in the same basis as the Lag. multipliers
      \item Precompute (parts of) most matrices and vectors
      \item Object-oriented design, lazy evaluation
      \item Optimized \textsc{Matlab} code
   \end{itemize}
\end{frame}


\subsection[Examples]{Numerical examples}

\begin{frame}
   \insertSubsectionTitle
   \begin{changemargin}{-1cm}{-2cm}
      \begin{itemize}
         \item Mode I tests
         \begin{itemize}
            \item three-point bending test
            \item ``classical'' Brazilian test
         \end{itemize}
      \end{itemize}
      \begin{minipage}[b]{0.85\textwidth}
         \begin{itemize}
            \item Brazilian test on a realistic microstructure
            \begin{itemize}
               \item thorough parametric study
               \item crack patterns and load-displacement curves qualitatively captured on coarse meshes and large step sizes
               \item importance of the boundary conditions and the heterogeneity
            \end{itemize}
         \end{itemize}
      \end{minipage}
      \begin{minipage}[b]{0.1\textwidth}
         \includegraphics[scale=0.7]{BrazilianRealization2.pdf}
      \end{minipage}
      \begin{minipage}[b]{0.85\textwidth}
         \begin{itemize}
            \item Uniaxial compression test
            \begin{itemize}
               \item primary and secondary shear bands
               \item similar observations as in the Brazilian test
            \end{itemize}
         \end{itemize}
      \end{minipage}
      \begin{minipage}[b]{0.1\textwidth}
         \includegraphics[scale=0.05]{failure2700.png}
      \end{minipage}
   \end{changemargin}
\end{frame}

% Thank you for your attention!
\insertThankYou[\Huge]

% References
\insertReferences


\end{document}
