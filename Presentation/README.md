# Presentation



This directory contains the presentations for my private and public defenses.



## Contents

- [Presentation structure](#presentation-structure)
- [Compiling](#compiling)



## Presentation structure

```
Presentation
|     README.md - this file
|     mypresentation.sty - Beamer presentation template
|     definitions.tex - definitions used in the presentations of my PhD defense
|--- private - ingredients for my private defense
|     |    privateDefense.tex - main source file
|     |    privateDefense.pdf - precompiled binary, optimized with pdfsizeopt
|     |--- Pictures - figures and images used in the presentation
|     |     |    ...
|--- public - ingredients for my public seminar and defense
|     |--- seminar
|     |     |    publicSeminar.tex - main source file
|     |     |    publicSeminar.pdf - precompiled binary, optimized with pdfsizeopt
|     |     |--- Pictures - figures and images used in the presentation
|     |     |     |    ...
|     |--- defense
|     |     |    publicDefense.tex - main source file
|     |     |    publicDefense.pdf - precompiled binary, optimized with pdfsizeopt
|     |     |--- Pictures - figures and images used in the presentation
|     |     |     |    ...
```



## Compiling

Precompiled binaries (pdf files) are shipped. If you want to compile yourself, do the following.

- Private defense:
    - using *arara*:
    
              arara privateDefense   
          
    - using standard tools:
    
              pdflatex privateDefense
              biber privateDefense
              pdflatex privateDefense
              pdflatex privateDefense
    
- Public seminar and defense: the same way as for the private defense
