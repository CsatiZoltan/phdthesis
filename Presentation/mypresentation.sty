% My preferred beamer presentation template

\ProvidesPackage{mypresentation}

%%%%%%%%%% Required packages %%%%%%%%%%
\def\Cplusplus{C\raisebox{0.3ex}{\small++} }

% Miscellaneous
\usepackage{siunitx} % consistent typesetting of numbers and their units
\usepackage[protrusion=true,expansion,verbose=true]{microtype} % micro-typographic extensions for beautiful typesetting; add the "draft" option to disable the package

% Maths
\usepackage{amsmath}
\usepackage{amsthm} % theorem-like environments
\usepackage{dsfont} % number sets
\usepackage{mathtools} % many packages need it
\usepackage[ISO]{diffcoeff} % simpler typing of derivatives
\usepackage{mleftright} % proper spacing around the \left and \right pairs

% Graphics
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{tikzscale} % scale graphics while keeping the text size
\usepackage[framemethod=TikZ]{mdframed} % automatically split frames
\usepackage{subcaption} % sub-figures
\usepackage{animate}
\usepackage[inactive,blur=0.6,fixcolor,preview]{fancytooltips}

% Spacing
\usepackage{float} % for the [H] option to enforce figure placement
\usepackage{setspace}
\usepackage{xspace} % automatically add space after a user-defined command if necessary
\usepackage{etoolbox} % low-level modification of Latex (I often used to change default spacing)

% Fonts
\usepackage{bm} % boldface Greek letters
\usepackage{textcomp} % Text Companion fonts; needed to get rid of the warnings of microtype when used with siunitx (https://tex.stackexchange.com/questions/373594/microtype-producing-dozens-of-unknown-slot-number-warnings#comment1150718_373607)
\usepackage{pifont} % for tick and cross marks for itemization

% Tables
\usepackage{booktabs} % nice tables

% Symbols
\usepackage{amssymb}
\usepackage{stmaryrd} % fancy brackets

% Bibliography
\usepackage[autocite=footnote, backend=biber, bibstyle=authoryear, maxcitenames=1, citestyle=authoryear, giveninits=true]{biblatex}

% Hyperlinks
\usepackage{url}
\usepackage{hyperref} % cross-referencing


%%%%%%%% Package options %%%%%%%%
% Hyperlinks
\hypersetup{linktoc=all, % everything is clickable in the TOC
   hidelinks,
   colorlinks=true, % do not have those ugly frames around the references
   citecolor=appropriateGreen, % color of the \cite commands
   linkcolor=appropriateRed, % color of the inner references (equation numbering, footnote, figures, etc)
   urlcolor=blue, % color of URL
   breaklinks=true, % break long URL
   pdfpagemode=FullScreen
}

% Bibliography
\DeclareNameAlias{author}{last-first} % https://tex.stackexchange.com/a/290104/119426
\setbeamertemplate{bibliography item}{\insertbiblabel} % show bibliography numbers

% Misc
\AtBeginEnvironment{verbatim}{\microtypesetup{activate=false}} % disable protrusion in verbatim environments
\DisableLigatures{encoding = T1, family = tt*} % disable ligatures for typewriter T1 encoded fonts
\SetSymbolFont{stmry}{bold}{U}{stmry}{m}{n} % avoid warning "Font shape `U/stmry/b/n' undefined(Font) using `U/stmry/m/n' instead" with the jump operator (from https://latex.org/forum/viewtopic.php?t=26024)


%%%%%%%%%% Beamer theme modifications %%%%%%%%%%

% Main theme
\mode<presentation>
{
   \usetheme{Ilmenau}
   \setbeamercovered{transparent} % or dynamic
   %  \useinnertheme{rectangles}
}

% Modified footline
\makeatletter
\setbeamertemplate{footline}
{
   \leavevmode%
   \hbox{%
      \begin{beamercolorbox}[wd=.1\paperwidth,ht=2.25ex,dp=1ex,center]
         {author in head/foot}%
         \usebeamerfont{author in head/foot}
         \insertshortauthor
      \end{beamercolorbox}%
      \begin{beamercolorbox}[wd=.8\paperwidth,ht=2.25ex,dp=1ex,center]
         {title in head/foot}%
         \usebeamerfont{title in head/foot}\insertshorttitle
      \end{beamercolorbox}%
      \begin{beamercolorbox}[wd=.1\paperwidth,ht=2.25ex,dp=1ex,center]
         {date in head/foot}%
         \usebeamerfont{date in head/foot}\insertframenumber{} / \inserttotalframenumber\hspace*{2em}
   \end{beamercolorbox}}%
   \vskip0pt%
}
\makeatother

%\setbeamertemplate{caption}{\insertcaption}
%\setbeamercolor{structure}{fg=yellow}
\usecolortheme{crane}

% Modify header
\setbeamertemplate{headline}
{%
   \begin{beamercolorbox}{section in head/foot}
      \insertsectionnavigationhorizontal{\textwidth}{}{}
   \end{beamercolorbox}%
}

% Remove the navigation symbols
\setbeamertemplate{navigation symbols}{}

% Math
\usefonttheme[onlymath]{serif}
\everymath{\displaystyle}

% Enable short section names in the toc and other places (http://tex.stackexchange.com/questions/203105/short-section-name-in-toc)
\makeatletter
\patchcmd{\beamer@section}{{#2}{\the\c@page}}{{#1}{\the\c@page}}{}{} % insert [short title] for \section in ToC
\patchcmd{\beamer@section}{{\the\c@section}{\secname}}{{\the\c@section}{#1}}{}{} % insert [short title] for \section in Navigation
\patchcmd{\beamer@subsection}{{#2}{\the\c@page}}{{#1}{\the\c@page}}{}{} % insert [short title] for \subsection in ToC
\patchcmd{\beamer@subsection}{{\the\c@subsection}{#2}}{{\the\c@subsection}{#1}}{}{} % insert [short title] for \subsection in Navigation
\makeatother



%%%%%%%%%% Definitions %%%%%%%%%%

% Colors
\definecolor{appropriateGreen}{RGB}{0,120,0}
\definecolor{appropriateRed}{RGB}{165,0,0}
\definecolor{azureblue}{RGB}{50, 215, 255}

% TODO notes (each for a for different purpose)
\newcommand{\addtodo}[1]{\todo[linecolor=appropriateGreen,backgroundcolor=appropriateGreen]{#1}}
\newcommand{\errortodo}[1]{\todo[linecolor=red,backgroundcolor=red]{#1}}
\newcommand{\uncertaintodo}[1]{\todo[linecolor=azureblue,backgroundcolor=azureblue]{#1}}

% Graphics
\newcommand*\circled[1]{\tikz[baseline=(char.base)]{\node[shape=circle,draw,inner sep=0.5pt] (char) {#1};}} % proper alignment for circled text
\newcommand{\mybox}[1]{\par\noindent\colorbox{shadecolor}{\parbox{\dimexpr\textwidth-2\fboxsep\relax}{#1}}} % https://tex.stackexchange.com/questions/136742/changing-background-color-of-text-in-latex

% Bibliography
% Citations in the footnote
\newcommand{\inlinecite}[1]{(\citeauthor{#1}, \citeyear{#1})}
% https://tex.stackexchange.com/a/26684/119426
\DeclareCiteCommand{\citejournal}
{\usebibmacro{prenote}}
{\usebibmacro{citeindex}%
   \usebibmacro{journal}}
{\multicitedelim}
{\usebibmacro{postnote}}
% Combining https://tex.stackexchange.com/a/116746/119426 with https://tex.stackexchange.com/a/123120/119426
\newcommand{\customfootcite}[1]{\footnote[frame]{\citeauthor{#1}, \citejournal{#1}, \citeyear{#1}}}
% The above command is used for citations, therefore set the corresponding citation color for the footnote mark
\renewcommand\thefootnote{\textcolor{appropriateGreen}{\arabic{footnote}}}

% Environment to locally change the page width (https://texfaq.org/FAQ-chngmargonfly)
\newenvironment{changemargin}[2]{%
   \begin{list}{}{%
         \setlength{\topsep}{0pt}%
         \setlength{\leftmargin}{#1}%
         \setlength{\rightmargin}{#2}%
         \setlength{\listparindent}{\parindent}%
         \setlength{\itemindent}{\parindent}%
         \setlength{\parsep}{\parskip}%
      }%
      \item[]}{
   \end{list}
}

% Utility commands to automate some parts of presentation I always use
% Title page
\newcommand{\insertTitlePage}{
   \begin{frame}
      \titlepage
   \end{frame}
}
% Table of contents
\newcommand{\insertTOC}{
   \begin{frame}
      \frametitle{\color{appropriateRed} Topics}
      \tableofcontents
   \end{frame}
}
% Frame title with section title
\newcommand{\insertSectionTitle}{
   \frametitle{\insertsection}
}
% Frame title with subsection title
\newcommand{\insertSubsectionTitle}{
   \frametitle{\insertsubsection}
}
% Frame title with both section and subsection titles
\newcommand{\insertSectionSubsectionTitle}{
   \frametitle{\insertsection\ -- \insertsubsection}
}
% References/bibliography
\newcommand{\insertReferences}{
   \appendix
   \begin{frame}[allowframebreaks]
      \frametitle{\color{appropriateRed} References}
      \printbibliography
   \end{frame}
}
% Thank-you frame
\newcommand{\insertThankYou}[1][\normalsize]{
   \begin{frame}
      \vfill%\vspace{0.5cm}
      \begin{center}
         {#1 Thank you for your attention!}
      \end{center}
      \vfill%\vspace{2cm}
   \end{frame}
}

% Tick and cross symbols (useful for itemization), https://tex.stackexchange.com/questions/42619/x-mark-to-match-checkmark
\newcommand{\cmark}{\ding{51}}
\newcommand{\xmark}{\ding{55}}
