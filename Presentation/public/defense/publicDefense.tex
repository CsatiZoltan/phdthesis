% To be used with the "mypresentation.sty" template

% !TEX root = publicDefense.tex

% The next four lines are directives for the arara TeX automation tool
% arara: pdflatex
% arara: pdflatex: {synctex: yes}

\documentclass[aspectratio=43]{beamer} % our projector has 4:3 aspect ratio; for widescreen use 169

\usepackage{../../mypresentation} % globally applicable packages, styles and definitions
\usepackage{media9}
\usepackage{multimedia}
\usepackage{algorithm}
\usepackage{algpseudocode} % more functionalities for typesetting algorithms

\graphicspath{{Pictures/}}

% Definitions
\input{../../definitions}

% No algorithm numbering
\renewcommand{\thealgorithm}{} % https://tex.stackexchange.com/a/107183

% Override the 'transparent' setting in mypresentation.sty
\mode<presentation>
{
   \setbeamercovered{invisible}
}

% Notes in Beamer
%\setbeameroption{show notes} % comment out to show only the main material

% Title page
\title[Mesh-independent modelling of diffuse cracking]{Mesh-independent modelling of diffuse cracking in cohesive grain-based materials \\[1em] {\small Public defense}}
\author{Zoltan Csati}
\date{
   Nantes \\
   21st October 2019 \\[2em]
   \begin{changemargin}{0cm}{0cm}
      \includegraphics[scale=0.06]{logoECN.pdf} \hfill \includegraphics[scale=0.07]{logoGeM.pdf} \hfill  \includegraphics[scale=0.25]{logoULB_noabbrv.png} \hfill \includegraphics[scale=0.055]{logoBATir.pdf}
   \end{changemargin}
}



\begin{document}

\insertTitlePage

\insertTOC[1]

% Main body of the presentation
\section[Motivation]{Motivation of the thesis}

\begin{frame}
	\insertSectionTitle
	\centering \includegraphics[scale=0.27]{ElCapitan.jpg} \\
	\centering \href{https://tinyurl.com/ybuseuph}{\scriptsize https://tinyurl.com/ybuseuph}
\end{frame}

\begin{frame}
	\insertSectionTitle
   \centering \includegraphics[scale=0.45]{graniteMicroscopy.png} \\
   \centering \scriptsize Fig.\ 2 from \href{https://dx.doi.org/10.1007/s12665-017-6696-4}{DOI: 10.1007/s12665-017-6696-4}
\end{frame}

\begin{frame}
	\insertSectionTitle
	\begin{changemargin}{-0.5cm}{-1cm}
	\begin{minipage}[t]{0.55\textwidth}
	   Experiments
	   \begin{itemize}
	   	\item[\cmark] reality
	   	\item[\xmark] occasionally costly
	   \end{itemize}
	\end{minipage}
%   \hfill
   \visible<2->{
   \begin{minipage}[t]{0.55\textwidth}
	   Numerical simulations
	   \begin{itemize}
	   	\item[\cmark] rapid prototyping
	   	\item[\cmark] parametric studies
	   	\item[\xmark] representative microstructure
	   	\item[\xmark] needs calibration
	   \end{itemize}
   \end{minipage}}
   \end{changemargin}
\end{frame}

\begin{frame}
   \insertSectionTitle
   \begin{changemargin}{-1cm}{-2cm}
      \begin{minipage}[b]{0.6\textwidth}
            \begin{itemize}
               \item Multiple cracking in cohesive grain-based materials
               \item Offer a computational tool for microstructural simulations
               \item Capture the relevant physics but be computationally efficient
               \item No user intervention
            \end{itemize}
      \end{minipage}
      \begin{minipage}[b]{0.35\textwidth}
         \visible<2->{\includegraphics[scale=0.3]{granite}}
      \end{minipage}
   	\skiponeline
      \visible<2->{\centering Studied material: granit}
   \end{changemargin}
\end{frame}

\section{About the title}

\begin{frame}
	\insertSectionTitle
	Cohesive grain-based materials
	\begin{figure}
		\centering
		\includegraphics[scale=0.5]{granite}
	\end{figure}
\end{frame}

\begin{frame}
	\insertSectionTitle
	Diffuse cracking
	\begin{figure}
		\centering
		\includegraphics[scale=0.22]{failure2700.png}
	\end{figure}
\end{frame}

\begin{frame}
	\insertSectionTitle
	Mesh-independent
	\begin{figure}
		\centering
		\visible<1->{
		\begin{subfigure}[t]{0.475\textwidth}
			\centering
			\includegraphics[scale=1]{conformingMesh.pdf}
		\end{subfigure}}
		\hfill
		\visible<2->{
		\begin{subfigure}[t]{0.475\textwidth}
			\centering
			\includegraphics[scale=1,width=4cm]{nonconformingMesh_refined_tikz.pdf}
		\end{subfigure}}
	\end{figure}
%   \hspace{0.2\textwidth}
   \begin{itemize}
   	\item<2->[\cmark] no meshing burden
   	\item<2->[\cmark] simpler data structures
   	\item<2->[\cmark] precompute the Jacobian matrix
   \end{itemize}
\end{frame}

\begin{frame}
	\begin{changemargin}{-1cm}{-1cm}
		\begin{minipage}[t]{0.55\textwidth}
			\begin{figure}
				\centering
				\includegraphics[scale=0.35]{FracturedBrazilianDisk.png} \\
				Fig. 15 (d) from \\ \href{https://dx.doi.org/10.1520/GTJ20170098}{DOI: 10.1520/GTJ20170098}
			\end{figure}
		\end{minipage}
	   \hfill
	   \begin{minipage}[t]{0.55\textwidth}
	      \begin{figure}
		      \centering
				\includegraphics[scale=0.17]{1failureRedscale21000.png}
	      \end{figure}
      \end{minipage}
      \visible<2>{\centering How can we get such simulation results?}
	\end{changemargin}
\end{frame}

\section{Workflow}

\begin{frame}
	\insertSectionTitle
	\begin{enumerate}
		\item Identify the most important phenomena we want to model
		\item Make use of the a priori known crack paths
		\item Construct a non-conforming discretization
		\item Ensure the stability of the scheme
		\item Verify the discretization
		\item Couple it with crack propagation
		\item Validate the model on various test cases
	\end{enumerate}
\end{frame}



\note{This talk is informal, giving as much intuition as I can. For the technical details, see the thesis. \\
      I also mention topics that are not found in the thesis, so this presentation is meant to compliment my thesis.}



\section{Physical model}

\begin{frame}
   \insertSectionTitle
   \begin{itemize}
      \item Complexity due to the microstructure
      \item Behaviour at the meso-scale
   \end{itemize}
   \begin{multicols*}{2}
   	\begin{figure}
   		\centering
   		\includegraphics<2>[scale=0.7]{workflow_1.pdf}
   		\includegraphics<3>[scale=0.7]{workflow_2.pdf}
   		\includegraphics<4>[scale=0.7]{workflow_3.pdf}
   		\includegraphics<5>[scale=0.7]{workflow_4.pdf}
   		\includegraphics<6>[scale=0.7]{workflow_5.pdf}
   	\end{figure}
   	\columnbreak
   	\only<2>{No crack}
   	\only<3>{Open cracks everywhere, floating grains}
   	\only<4>{Lagrange multipliers ``glue'' the grains together}
   	\only<5>{To much ``glue'' causes instability}
   	\only<6>{Allow gradual crack opening, cohesive zone}
   \end{multicols*}
\end{frame}



\section{Contributions}

\begin{frame}
	\insertSectionTitle
	\begin{itemize}
		\item CutFEM with Lagrange multipliers for Q1 elements with arbitrary discontinuities
		\item Mixed-mode crack propagation
      \item Non-uniform fracture energy
		\item Damage-driven solution procedure
		\item Robust discretization and crack propagation
	\end{itemize}
\end{frame}


\section{Kinematics}

\begin{frame}
   \insertSectionTitle
   Discrete crack model
   \skiponeline
   \begin{minipage}[c]{0.4\textwidth}
   	\begin{figure}
   		\centering
   		\includegraphics[scale=0.6]{uncrackedContinuum_loading.pdf}
   	\end{figure}
   \end{minipage}
   \hfill
   \begin{minipage}[c]{0.1\textwidth}
      \visible<2->{$\Longrightarrow$}
   \end{minipage}
   \hfill
   \begin{minipage}[c]{0.4\textwidth}
      \visible<2->{\centering \includegraphics[scale=0.6]{crackedContinuum_loading.pdf}}
   \end{minipage}
	\skiponeline
	\visible<3->{Discontinuity in the displacement field: $\jump{\mathbf{u}} \neq \mathbf{0}$ \\}
	\visible<3->{Suppress discontinuity for cracks not yet appeared}
\end{frame}



\section{Governing equations}

\begin{frame}
	\insertSectionTitle
	\begin{minipage}[t]{0.45\textwidth}
		\begin{figure}
			\centering
			\includegraphics[scale=1]{uncrackedContinuumPolygon.pdf}
		\end{figure}
	   \begin{equation*}
	      \begin{aligned}
	         \bm{\sigma}\cdot\nabla = \mathbf{0}, \quad & \mathbf{x} \in \Omega \\
	         \bm{\sigma} = \bm{\mathcal{C}}:\bm{\varepsilon}, \quad & \mathbf{x} \in \Omega \\
	         \bm{\varepsilon} = \nabla^s\mathbf{u}, \quad & \mathbf{x} \in \Omega
	      \end{aligned}
	   \end{equation*}
	   \begin{equation*}
	      \begin{alignedat}{2}
	         \mathbf{u} &= \mathbf{u}_D, \quad &&\mathbf{x}\in\Gamma_D \\
	         \bm{\sigma}\cdot\mathbf{n} &= \mathbf{t}_N, \quad &&\mathbf{x}\in\Gamma_N
	      \end{alignedat}
	   \end{equation*}
	\end{minipage}
	\hfill
   \begin{minipage}[t]{0.45\textwidth}
   	\visible<2->{
   	\begin{figure}
   		\centering
   		\includegraphics[scale=1]{uncrackedContinuumDecomposedPolygon.pdf}
   	\end{figure}}
      \visible<2->{
      \begin{equation*}
         \begin{aligned}
            \bm{\sigma}^m\cdot\nabla = \mathbf{0}, \quad \mathbf{x} \in \Omega^m \\
            \bm{\sigma}^m = \bm{\mathcal{C}}^m:\bm{\varepsilon}^m, \quad \mathbf{x} \in \Omega^m \\
            \bm{\varepsilon}^m = \nabla^s\mathbf{u}^m, \quad \mathbf{x} \in \Omega^m
         \end{aligned}
      \end{equation*}}
      \visible<2->{\vspace{-2em}
      \begin{alignat*}{2}
         \mathbf{u}^i &= \mathbf{u}^i_D, \quad &&\mathbf{x}\in\Gamma_D^i \\
         \bm{\sigma}^i\cdot\mathbf{n}^i &= \mathbf{t}^i_N, \quad &&\mathbf{x}\in\Gamma_N^i
      \end{alignat*}}
      \visible<2->{\vspace{-2em}
      \begin{equation*}
         \jump{\mathbf{u}}^i = \mathbf{0}, \quad \jump{\bm{\sigma}}^i\cdot\mathbf{n}^i = \mathbf{0}, \quad \mathbf{x}\in\Gamma^i
      \end{equation*}}
   \end{minipage}
\end{frame}

\begin{frame}
	\insertSectionTitle
	\begin{block}{Weak form}
		Find $\mathbf{u} \in V$ and $\bm{\lambda} \in \Lambda$ such that
		\begin{equation*}
			\begin{alignedat}{3}
			   &a(\mathbf{u}, \mathbf{v}) + b(\bm{\lambda}, \mathbf{v}) &&= f(\mathbf{v}), \quad &&\forall \mathbf{v} \in V \\
			   &b(\mathbf{u}, \bm{\mu}) && = g(\bm{\mu}), \quad &&\forall {\bm{\mu}} \in \Lambda
			\end{alignedat}
		\end{equation*}
	\end{block}
\end{frame}



\section{Discretization}

\begin{frame}
	\insertSectionTitle
	\begin{block}{Discrete weak form}
		Find $\mathbf{u}_h \in V_h$ and $\bm{\lambda}_h \in \Lambda_h$ such that
		\begin{equation*}
		   \begin{alignedat}{3}
		      &a_h(\mathbf{u}_h, \mathbf{v}_h) + b_h(\bm{\lambda}_h, \mathbf{v}_h) &&= f_h(\mathbf{v}_h), \quad &&\forall \mathbf{v}_h \in V_h \\
		      &b_h(\mathbf{u}_h, \bm{\mu}_h) && = g_h(\bm{\mu}_h), \quad &&\forall {\bm{\mu}_h} \in \Lambda_h
		   \end{alignedat}
		\end{equation*}
	\end{block}
   \visible<2->{Requirements}
   \begin{itemize}
   	\item<2-> FEM
   	\item<2-> Decouple mesh from domain
   	\item<2-> Stability
   \end{itemize}
   \visible<3->{
   	\vspace{1em}
   	Adapted method
   	\begin{itemize}
   		\item CutFEM
   		\item Lagrange multipliers
   		\item No interfacial mesh
   	\end{itemize}}
\end{frame}

\subsection*{CutFEM}

\begin{frame}
	\insertSubsectionTitle
	\begin{figure}
		\centering
		\includegraphics<1>[scale=0.8]{traceBulkMesh_1.pdf}
		\includegraphics<2>[scale=0.8]{traceBulkMesh_2.pdf}
		\includegraphics<3>[scale=0.8]{traceBulkMesh_3.pdf}
		\includegraphics<4->[scale=0.8]{traceBulkMesh_4.pdf}
%		\includegraphics[scale=0.8]{traceBulkMesh.pdf}
	\end{figure}
   \visible<3->{
   \begin{equation*}
      V_h^i \defeq \spa \{ \hat{\bm{\psi}}^i_j \suchthat j\in \mathcal{M}^i \}
   \end{equation*}}
   \visible<4->{
   \begin{equation*}
      V_h = \bigoplus\limits_{i \in \Isubdom} V_h^i
   \end{equation*}}
\end{frame}

\begin{frame}
	\insertSubsectionTitle
	\begin{figure}
		\centering
		\includegraphics[scale=0.8]{traceSurfaceMesh.pdf}
	\end{figure}
   \begin{equation*}
      \Lambda_h^i = \spa \mleft\{ \tilde{\bm{\psi}}^i_j \suchthat j \in \tilde{\mathcal{M}}^i_{\Gamma} \mright\} 
   \end{equation*}
   \begin{equation*}
      \tilde{\bm{\psi}}^i_j = \sum\limits_{k \in \mathcal{M}^i_{\Gamma}} \alpha_{jk}\bm{\psi}_k|_{\Gamma^i}
   \end{equation*}
   \begin{equation*}
      \Lambda_h = \bigoplus\limits_{\mathclap{i \in \Iint \cup \IDir}} \Lambda_h^i
   \end{equation*}
\end{frame}

\subsection*{Implementation}

\begin{frame}
	\insertSubsectionTitle
	\begin{figure}
		\centering
		\includegraphics<1>[scale=0.8]{phantomNodes_0.pdf}
		\includegraphics<2>[scale=0.8]{phantomNodes_1.pdf}
		\includegraphics<3>[scale=2]{phantomNodes_2.pdf}
		\includegraphics<4>[scale=2]{phantomNodes_3.pdf}
		\includegraphics<5>[scale=2]{phantomNodes_4.pdf}
		\includegraphics<6>[scale=2]{phantomNodes_5.pdf}
		\includegraphics<7>[scale=2]{phantomNodes_6.pdf}
		\includegraphics<8->[scale=2]{phantomNodes_7.pdf}
	\end{figure}
\end{frame}



\section{Stable mixed method}

\subsection*{Concepts}

\begin{frame}
	\insertSubsectionTitle
	\begin{itemize}
		\item Stability
		\item Goal
		\begin{itemize}
			\item ensure stability
			\item decrease $\dim \Lambda_h$
			\item easy-to-implement procedure
		\end{itemize}
	\end{itemize}
\end{frame}

%\subsection*{Naive approach}
%
%\begin{frame}
%	\insertSubsectionTitle
%	\begin{figure}
%		\centering
%		\includegraphics[scale=0.8]{naiveInterfaceMesh.pdf}
%	\end{figure}
%   \visible<2->{Problems
%   \begin{itemize}
%   	\item requires interfacial mesh
%   	\item not stable
%   \end{itemize}}
%\end{frame}
%
%\begin{frame}
%	\insertSubsectionTitle
%	\begin{minipage}[c]{0.4\textwidth}
%		\begin{figure}
%			\centering
%			\includegraphics[scale=1]{naiveInterfaceMesh2.pdf}
%		\end{figure}
%	\end{minipage}
%   \hfill
%   \visible<2->{
%	\begin{minipage}[c]{0.4\textwidth}
%		\begin{itemize}
%			\item<2-> still not stable
%			\item<3> lack of completeness
%		\end{itemize}
%	\end{minipage}}
%\end{frame}

\subsection*{The reduction algorithm}

\begin{frame}
	\insertSubsectionTitle
	\begin{figure}
		\centering
		\includegraphics<1>[scale=2]{tooRich.pdf}
		\includegraphics<2>[scale=2]{optimal.pdf}
		\includegraphics<3>[scale=2]{wrongTying.pdf}
		\includegraphics<4>[scale=2]{correctTying.pdf}
	\end{figure}
\end{frame}

\subsection*{Assessing the stability}

\begin{frame}
	\insertSubsectionTitle
	\begin{figure}
		\centering
		\includegraphics<1>[scale=1]{LBBConstant.pdf}
		\includegraphics<2>[scale=1]{coercivityConstant.pdf}
	\end{figure}
\end{frame}



\section[Cracking]{Crack propagation model}

\subsection*{Cohesive zone model}

\begin{frame}
	\insertSubsectionTitle
	\begin{itemize}
		\item Discrete crack model
		\item Cohesive zone model
	\end{itemize}
	\begin{figure}
		\centering
		\includegraphics<1>[scale=0.8]{cohesiveZone_1.pdf}
		\includegraphics<2>[scale=0.8]{cohesiveZone_2.pdf}
		\includegraphics<3>[scale=0.8]{cohesiveZone_3.pdf}
	\end{figure}
\end{frame}

%\note{Idea of the cohesive zone model:}

\begin{frame}
	\insertSubsectionTitle
	\begin{figure}
		\centering
		\begin{subfigure}[t]{0.4\textwidth}
			\centering
			\includegraphics[scale=1]{intrinsic.pdf}
			\caption{Intrinsic CZM}
		\end{subfigure}
	   \hfill
	   \visible<2>{
		\begin{subfigure}[t]{0.4\textwidth}
			\centering
			\includegraphics[scale=1]{extrinsic.pdf}
			\caption{Extrinsic CZM}
		\end{subfigure}}
	\end{figure}
   \hypertarget{extrinsicCZM}{}
   \only<2>{\hfill \hyperlink{freeEnergy}{\beamerreturnbutton{return}}}
\end{frame}

\subsection*{Failure criterion}

\begin{frame}
	\insertSubsectionTitle
	\begin{changemargin}{-1cm}{-1cm}
		\begin{figure}
			\centering
			\includegraphics[scale=0.75]{MCmodelLoadingModes.pdf}
		\end{figure}
   \end{changemargin}
\end{frame}

\subsection*{Mixed method for the cohesive formulation}

\begin{frame}
	\insertSubsectionTitle
	\begin{changemargin}{-1cm}{-2cm}
      \raggedcolumns
		\begin{multicols*}{2}
			\begin{itemize}[<+->]
				\item No crack
				\begin{equation*}
				   \scalebox{0.8}{$\int\limits_{\Gamma} \bm{\mu}\cdot \jump{\mathbf{u}} \DIFF \Gamma = 0$}
				\end{equation*}
				\item Cohesive crack
				\begin{equation*}
				   \scalebox{0.8}{$\int\limits_{\Gamma} \bm{\mu}\cdot \mleft( \jump{\mathbf{u}} - \alert{\jump{\mathbf{u}} (\bm{\lambda})} \mright) \DIFF \Gamma = 0$}
				\end{equation*}
				\item Contact
				\begin{equation*}
				   \scalebox{0.8}{$\int\limits_{\Gamma} \bm{\mu}\cdot \mleft( \jump{\mathbf{u}} - \alert{\chi(t_n > 0)}\jump{\mathbf{u}} (\bm{\lambda}) \mright) \DIFF \Gamma = 0$}
				\end{equation*}
%				\item Compliance term
%				\item Modified Lagrange multiplier
%				\begin{equation*}
%				   \scalebox{0.8}{$\bm{\zeta} = \bm{\lambda} + \mathbf{k} \cdot \jump{\mathbf{u}}$}
%				\end{equation*}
				\item Final form
				\begin{equation*}
				   \scalebox{0.8}{$\int\limits_{\Gamma} \alert{\bm{\eta}}\cdot \mleft( \jump{\mathbf{u}} - \chi(\alert{\zeta_n} > 0)\jump{\mathbf{u}} (\alert{\bm{\zeta}}) \mright) \DIFF \Gamma = 0$}
				\end{equation*}
			\end{itemize}
	   \columnbreak
	   \begin{figure}
	   	\includegraphics<1->[scale=0.9]{noCrack.pdf}
	   \end{figure}
   	\begin{figure}
   		\includegraphics<2->[scale=0.9]{loadedCohesiveZone.pdf}
   	\end{figure}
   	\begin{figure}
   		\includegraphics<3->[scale=0.9]{contactCohesiveZone.pdf}
   	\end{figure}
   \end{multicols*}
   \end{changemargin}
\end{frame}

\subsection*{Internal variable}

\begin{frame}
	\insertSubsectionTitle
	\begin{itemize}[<+->]
		\item Damage-driven computation
		\item Free energy
		\begin{equation*}
		   \varphi(\jump{\mathbf{u}}, d) = \frac{1}{2}\mleft( \frac{1}{d} - 1 \mright) \jump{\mathbf{u}}\cdot\mathbf{k}\cdot\jump{\mathbf{u}}
		\end{equation*}
		\item Cohesive traction
		\begin{gather*}
		   \mathbf{t} = \diffp{\varphi}{\jump{\mathbf{u}}} = \mleft( \frac{1}{d} - 1 \mright)\mathbf{k}\cdot\jump{\mathbf{u}} \\
		   \only<3>{\hyperlink{extrinsicCZM}{\beamergotobutton{cf. extrinsic CZM}}}
		   \only<3>{\hypertarget{freeEnergy}{}}
		\end{gather*}
		\item Energy release rate
		\begin{equation*}
		   y = -\diffp{\varphi}{d} = \frac{1}{2d^2}\jump{\mathbf{u}}\cdot\mathbf{k}\cdot\jump{\mathbf{u}}
		\end{equation*}
		\item Weak constraint
		\begin{equation*}
		   \int\limits_{\Gamma} \bm{\eta}\cdot \mleft( \jump{\mathbf{u}} - \chi(\zeta_n > 0)\alert{d\mathbf{k}^{-1}}\cdot\bm{\zeta} \mright) \DIFF \Gamma = 0
		\end{equation*}
	\end{itemize}
\end{frame}

\subsection*{Damage evolution}

\begin{frame}
	\insertSubsectionTitle
	\begin{itemize}
		\item Introduce energy dissipation
		\begin{align*}
		   \dot{G} & \geq 0 \\
		   y-y_c(G) & \leq 0 \\
		   \mleft( y-y_c(G) \mright)\dot{G} &= 0
		\end{align*}
		\item Discretization $\rightarrow$ compute $\Delta G$
		\item Update $\Delta d$
	\end{itemize}
\end{frame}

\subsection*{Solution procedure}

\begin{frame}
	\insertSubsectionTitle
	\begin{algorithm}[H]
		\caption{Quasi-static simulation}
		\begin{algorithmic}[1]
			\State Precompute matrices and vectors
			\State Initialize the damage field $\rightarrow$ $d^{(0)}$
			\For {$n$ \textbf{from} $1$ \textbf{to} $N_\mathrm{step}$} \Comment{damage stepping loop}
			   \State Update the ``mass'' matrix
			   \State Solve the mechanical problem $\rightarrow$ $\mathbf{u}^{(n)}, \bm{\zeta}^{(n)}$
			   \State Determine the energy dissipation $\rightarrow$ $G^{(n+1)}$
			   \State Update the damage field $\rightarrow$ $d^{(n+1)}$
			\EndFor
		\end{algorithmic}
	\end{algorithm}
\end{frame}



\section[Examples]{Test examples}

\begin{frame}
   \insertSectionTitle
   \begin{itemize}
   	\item<1-> Verification
   	\item<1-> Validation
   	\item<2-> Examples
   	\begin{itemize}
   		\item Three-point bending test
   		\item Brazilian test
   		\item Brazilian test with heterogeneity
   		\item Compression test
   	\end{itemize}
   \end{itemize}
\end{frame}

\subsection*{Three-point bending test}

\begin{frame}
	\insertSubsectionTitle
	\begin{figure}
		\centering
		\includegraphics[scale=1]{threePointBendingGeometry.pdf}
	\end{figure}
\end{frame}

\begin{frame}
	\insertSubsectionTitle
	\begin{figure}
		\centering
		\includegraphics[scale=0.8]{threePointBendingLoadDisplacement.pdf}
	\end{figure}
\end{frame}

\subsection*{Brazilian test}

\begin{frame}
	\insertSubsectionTitle
	\begin{minipage}[c]{0.55\textwidth}
		\begin{itemize}
			\item Proposed in 1943
			\item Determine the tensile strength
			\item Widespread in rock testing
			\item Closed-form solution exists
		\end{itemize}
	\end{minipage}
   \hfill
   \begin{minipage}[c]{0.4\textwidth}
   	\begin{figure}
   		\centering
   		\includegraphics[scale=1]{BrazilianTestExperiment.jpg}
   	\end{figure}
   	\centering \href{https://tinyurl.com/yxd49ulo}{\scriptsize https://tinyurl.com/yxd49ulo}
%   	\includemedia[width=0.35\textwidth, height=0.6\linewidth, activate=pageopen, addresource=BrazilianTestVideo.mp4, flashvars={source=BrazilianTestVideo.mp4}]{}{VPlayer.swf} \\
%   	\href{https://www.youtube.com/watch?v=6lkZIrLp_mE}{https://www.youtube.com/watch?v=6lkZIrLp_mE}
   \end{minipage}
%   \movie{h}{BrazilianTestVideo.avi}
\end{frame}

\begin{frame}
	\insertSubsectionTitle
	\begin{figure}[h]
		\centering
		\begin{subfigure}[t]{0.475\textwidth}
			\centering
			\includegraphics[scale=0.7]{brazilianGeom+Mesh.pdf}
			\caption{Numerical model}
			\label{fig:brazilianGeom+Mesh}
		\end{subfigure}
		\hfill
		\visible<2>{
		\begin{subfigure}[t]{0.475\textwidth}
			\centering
			\includegraphics[scale=0.7]{brazilianRelativeError.pdf}
			\caption{Relative error in the critical load}
			\label{fig:brazilianRelativeError}
		\end{subfigure}}
	\end{figure}
\end{frame}

\begin{frame}
	\insertSubsectionTitle
	\begin{figure}
		\centering
		\includegraphics[scale=0.75]{brazilianTractionProfiles.pdf}
	\end{figure}
\end{frame}

\begin{frame}
	\insertSubsectionTitle
	\begin{figure}
		\centering
		\begin{subfigure}[b]{0.3\textwidth}
			\centering 
			\includegraphics[scale=0.7]{K101Delted01.pdf}
			\caption{$\Delta d_{\max} = \num{0.1}$}
		\end{subfigure}
		\hfill
		\begin{subfigure}[b]{0.3\textwidth}
			\centering
			\includegraphics[scale=0.7]{K101Delted001.pdf}
			\caption{$\Delta d_{\max} = \num{0.01}$}
		\end{subfigure}
		\hfill
		\begin{subfigure}[b]{0.3\textwidth}
			\centering 
			\includegraphics[scale=0.7]{K101Delted0001.pdf}
			\caption{$\Delta d_{\max} = \num{0.001}$}
		\end{subfigure}
	\end{figure}
\end{frame}

\begin{frame}
	\insertSubsectionTitle
	\begin{figure}
		\centering
		\includegraphics[scale=0.8]{BrazilianGeometryMesh50x50.pdf}
	\end{figure}
\end{frame}

\begin{frame}
	\insertSubsectionTitle
	\begin{figure}
		\centering
		\includegraphics[scale=1]{BrazilianLoadDispft.pdf}
	\end{figure}
\end{frame}

\begin{frame}
	\insertSubsectionTitle
	\begin{figure}[h]
		\centering
		\begin{subfigure}[b]{0.3\textwidth}
			\centering 
			\includegraphics[scale=0.1]{failure7600.png}
			\caption{$\Delta\tilde{G}_{\max} = \num{1e-1}$}
			\label{fig:stepSize01_failure}
		\end{subfigure}
		\hfill
		\begin{subfigure}[b]{0.3\textwidth}
			\centering
			\includegraphics[scale=0.1]{failure37500.png}
			\caption{$\Delta\tilde{G}_{\max} = \num{1e-2}$}
			\label{fig:stepSize001_failure}
		\end{subfigure}
		\hfill
		\begin{subfigure}[b]{0.3\textwidth}
			\centering 
			\includegraphics[scale=0.1]{failure96000.png}
			\caption{$\Delta\tilde{G}_{\max} = \num{1e-3}$}
			\label{fig:stepSize0001_failure}
		\end{subfigure}
	\end{figure}
\end{frame}

\begin{frame}
	\insertSubsectionTitle
	\begin{changemargin}{-1cm}{-1cm}
		\begin{figure}
			\centering
			\includegraphics[scale=0.1]{1failureRedscale21000.png}
			\includegraphics[scale=0.1]{3failureRedscale5000.png}
			\includegraphics[scale=0.1]{4failureRedscale4000.png}
			\includegraphics[scale=0.1]{5failureRedscale7500.png} \\[1em]
			\includegraphics[scale=0.1]{6failureRedscale6000.png}
			\includegraphics[scale=0.1]{8failureRedscale5000.png}
			\includegraphics[scale=0.1]{9failureRedscale6000.png}
			\includegraphics[scale=0.1]{10failureRedscale3400.png}
	\end{figure}
   \end{changemargin}
\end{frame}

\subsection*{Compression test}

\begin{frame}
	\insertSubsectionTitle
	\begin{itemize}
		\item Determine the compressive strength
		\item Widespread in rock testing
	\end{itemize}
	\begin{figure}
		\centering
		\includegraphics[scale=1]{UniaxialTestExperiment.jpg}
	\end{figure}
	\centering \href{https://dx.doi.org/10.1061/(ASCE)0899-1561(2009)21\%3A9(502)}{\scriptsize DOI: 10.1061/(ASCE)0899-1561(2009)21\%3A9(502)}
\end{frame}

\begin{frame}
	\insertSubsectionTitle
	\begin{figure}
		\centering
		\includegraphics[scale=0.7]{UniaxialModel.pdf}
	\end{figure}
\end{frame}

\begin{frame}
	\insertSubsectionTitle
	\begin{figure}
		\centering
		\begin{subfigure}[b]{0.4\textwidth}
			\centering
			\includegraphics[scale=0.2]{failureRedscale7800.png}
		\end{subfigure}
		\qquad
		\visible<2>{
		\begin{subfigure}[b]{0.4\textwidth}
			\centering
			\includegraphics[scale=0.2]{failureRedscale11100.png}
		\end{subfigure}}
	\end{figure}
\end{frame}

\begin{frame}
	\insertSubsectionTitle
	\begin{figure}
		\centering
		\begin{subfigure}[b]{0.22\textwidth}
			\centering 
			\includegraphics[scale=0.13]{failure1200.png}
			\caption{Snapshot A}
		\end{subfigure}
		\hfill
		\begin{subfigure}[b]{0.22\textwidth}
			\centering
			\includegraphics[scale=0.13]{failure2700.png}
			\caption{Snapshot B}
		\end{subfigure}
		\hfill
		\begin{subfigure}[b]{0.22\textwidth}
			\centering 
			\includegraphics[scale=0.13]{failure6300.png}
			\caption{Snapshot C}
		\end{subfigure}
		\hfill
		\begin{subfigure}[b]{0.22\textwidth}
			\centering 
			\includegraphics[scale=0.13]{failure11100.png}
			\caption{Final crack}
		\end{subfigure}
	\end{figure}
\end{frame}



\section{Conclusions}

\begin{frame}
	\insertSectionTitle
	\begin{itemize}
		\item Two-field variational formulation
		\item All DOF defined at the nodes of a Cartesian mesh
		\item Reduction algorithm on Lagrange multipliers
		\item Arbitrary discontinuities in Q1 elements
		\item Composite failure criterion
		\item Non-uniform fracture energy
		\item Damage-driven solution procedure
		\item Verification and parameter studies
		\item Qualitatively correct results on coarse meshes and with large damage steps
	\end{itemize}
\end{frame}



% Thank you for your attention!
\insertThankYou[\Huge]

% Extra slides
\appendix
\setbeamertemplate{footline}{}
\section*{The reduction algorithm in details}

\begin{frame}[noframenumbering]
	\insertSectionTitle
	\begin{figure}
		\centering
		\includegraphics<1>[scale=1]{LagMultTying_junction_1.pdf}
		\includegraphics<2>[scale=1]{LagMultTying_junction_2.pdf}
		\includegraphics<3>[scale=1]{LagMultTying_junction_3.pdf}
		\includegraphics<4>[scale=1]{LagMultTying_junction_4.pdf}
		\includegraphics<5>[scale=1]{LagMultTying_junction_5.pdf}
		\includegraphics<6>[scale=1]{LagMultTying_junction_6.pdf}
		\includegraphics<7>[scale=1]{LagMultTying_junction_7.pdf}
   	\includegraphics<8>[width=0.28\linewidth]{LagMultShapeFcn_junction.tikz}
	\end{figure}
\end{frame}

\section*{Assembling the coupling matrix}

\begin{frame}[noframenumbering]
	\insertSectionTitle
	\begin{figure}
		\centering
		\includegraphics<1>[scale=1]{LagMultTying_junction_8.pdf}
		\includegraphics<2>[scale=1]{LagMultTying_junction_9.pdf}
		\includegraphics<3>[scale=1]{LagMultTying_junction_10.pdf}
		\includegraphics<4>[scale=1]{LagMultTying_junction_11.pdf}
	\end{figure}
\end{frame}


\end{document}
