\graphicspath{{Chapters/ProblemFormulation/Pictures/}}

\chapter{Problem formulation and discretization} \label{ch:ProblemFormulation}
\localtableofcontents

\input{ChapterSummaries/ProblemFormulation-summary}
\input{ChapterSummaries/Discretization-summary} % the jury asked to merge this chapter with the "Discretization"

\section{Definitions}

Let us consider the domain $\Omega \subset \mathds{R}^d\ (d=2,3)$ partitioned into $M$ non-overlapping \emph{subdomain}s $\Omega^i$ of polytope shapes, identified by the index set $\Isubdom = \{ 1, \ldots, M\}$ such that
\begin{equation}
   \overline{\Omega} = \bigcup\limits_{i \in \Isubdom} \overline{\Omega^i}, \qquad \Omega^i \bigcap \Omega^j = \varnothing,\ \forall i,j\in \Isubdom,\ i \neq j
\end{equation}
where the overline denotes the closure of a set.
Without the loss of generality, it is assumed that $M > 1$.
When we talk about the physical nature of the problem, we will use the word \emph{grain} instead of subdomain.
A straight segment between two subdomains is called an \emph{interface} and is denoted by $\Gamma^i$.
Note that two subdomains can have multiple common interfaces.
The interfaces are indexed by $\Iint = \{ 1, \ldots, N \}$.
The neighbouring subdomains to $\Gamma^i$ are denoted by $\Omega^{i+}$ and $\Omega^{i-}$, or $\Omega^{i\pm}$ when dealing with them as a collection.
Parts of the boundary of $\Omega$ where non-zero Neumann boundary conditions (BC) are prescribed are denoted by $\Gamma_N$, whereas $\Gamma_D$ denotes parts where zero or non-zero Dirichlet BCs are given.
It is assumed that $\Gamma_N \cap \Gamma_D = \varnothing$.
These boundary parts are replaced by straight segments $\Gamma^i_N$ ($i\in \INeu \subseteq \Isubdom$) and $\Gamma^i_D$ ($i\in \IDir \subseteq \Isubdom$) such that
\begin{gather}
   \Gamma^i_N = \Gamma_N \bigcap \overline{\Omega^i}, \qquad \bigcup\limits_{i \in \INeu} \Gamma^i_N = \Gamma_N, \\
   \Gamma^i_D = \Gamma_D \bigcap \overline{\Omega^i}, \qquad \bigcup\limits_{i \in \IDir} \Gamma^i_D = \Gamma_D.
\end{gather}
The subdomain boundaries are oriented counter-clockwise and the interface and boundary segments ($\Gamma^i$ and $\Gamma^i_N$, $\Gamma^i_D$) are oriented according to their outward unit normal attached to $\Omega^{i+}$.
In other words, the orientation of the subdomain determines the normal and tangent unit vectors of its boundary, and an interface inherits these unit vectors from the first neighbouring subdomain it belongs to.
These notations are visualized in Fig.~\ref{fig:grainGeometry} in a general situation.
The intuition behind these definitions is the observation that the boundary conditions and the interface conditions can be handled within a unified framework which is advantageous when the constraint equations are weakly enforced, as will be explained in Section~\ref{sec:mixedContinuous}.
\begin{figure}[H]
	\centering
	\includegraphics[scale=1]{grainGeometry.pdf}
	\caption[Notations for the geometry]{Notations used for the geometrical description of the domain of interest}
	\label{fig:grainGeometry}
\end{figure}



\section{Governing equations}

In this work, isotropic linear elastic grains are considered.
Note that this assumption is not critical and that grain anisotropy could be considered.
The equilibrium, constitutive and kinematic equations for these grains are therefore given as Eq.~\eqref{eq:bulkEquations}:
\begin{equation}
   \begin{aligned}
      \bm{\sigma}\cdot\nabla = \mathbf{0}, \quad & \mathbf{x} \in \Omega \\
      \bm{\sigma} = \bm{\mathcal{C}}:\bm{\varepsilon}, \quad & \mathbf{x} \in \Omega \\
      \bm{\varepsilon} = \nabla^s\mathbf{u} \quad & \mathbf{x} \in \Omega.
   \end{aligned}
   \label{eq:bulkEquations}
\end{equation}
To simplify the presentation, body forces are neglected.
The Hooke tensor $\bm{\mathcal{C}}$ can vary from grain to grain if the material residing in $\Omega$ is heterogeneous.
The boundary conditions are written as
\begin{equation}
   \begin{alignedat}{2}
      \mathbf{u} &= \mathbf{u}_D, \quad &&\mathbf{x}\in\Gamma_D, \\
      \bm{\sigma}\cdot\mathbf{n} &= \mathbf{t}_N, \quad &&\mathbf{x}\in\Gamma_N,
   \end{alignedat}
   \label{eq:BCcontinuous}
\end{equation}
where $\mathbf{n}$ is the outward unit normal to the boundary.
Because of the (cohesive) granular structure of the assumed material, it is straightforward to perform the calculation grain-wise as done in~\cite{Simone2006}.
To this end, the displacement field is decomposed as
\begin{equation}
   \mathbf{u} = \sum\limits_{m \in \Isubdom}\chi^m\mathbf{u}^m,
   \label{eq:displacementDecomposition}
\end{equation}
where $\chi^m$ is the indicator function for grain $m$ and is defined as
\begin{equation}
   \chi^m(\mathbf{x}) \defeq
   \begin{cases}
      1 & \mathbf{x} \in \Omega^m \\
      0 & \text{otherwise}.
   \end{cases}
\end{equation}
The boundary value problem Eq.~\eqref{eq:bulkEquations}--\eqref{eq:BCcontinuous} is then written for each grain as:
\begin{equation}
   \begin{aligned}
      \bm{\sigma}^m\cdot\nabla = \mathbf{0}, \quad \mathbf{x} \in \Omega^m \\
      \bm{\sigma}^m = \bm{\mathcal{C}}^m:\bm{\varepsilon}^m, \quad \mathbf{x} \in \Omega^m \\
      \bm{\varepsilon}^m = \nabla^s\mathbf{u}^m, \quad \mathbf{x} \in \Omega^m
   \end{aligned}
   \label{eq:bulkEquationsSubdomains}
\end{equation}
\begin{alignat}{2}
   \mathbf{u}^i &= \mathbf{u}^i_D, \quad &&\mathbf{x}\in\Gamma_D^i, 
   \label{eq:BCDir} \\
   \bm{\sigma}^j\cdot\mathbf{n}^j &= \mathbf{t}^j_N, \quad &&\mathbf{x}\in\Gamma_N^j,
   \label{eq:BCNeu}
\end{alignat}
$\forall m \in \Isubdom$, $\forall i \in \IDir$, $\forall j \in \INeu$.	In addition, we need to provide the continuity of the primary variable (the displacement) and its derivative along the interfaces.
Physically, this means prescribing along each interface the continuity of the displacement field
\begin{equation}
   \mleft. \mathbf{u}^{i+} \mright|_{\Gamma^i} = \mleft. \mathbf{u}^{i-} \mright|_{\Gamma^i}
\end{equation}
and of the interfacial traction
\begin{align}
   \mleft. \mathbf{t}^{j+} \mright|_{\Gamma^j} &= \mleft. \mathbf{t}^{j-} \mright|_{\Gamma^j} \\
   \mleft. \bm{\sigma}^{j+}\cdot\mathbf{n}^j \mright|_{\Gamma^j} &= \mleft. \bm{\sigma}^{j-}\cdot\mathbf{n}^j \mright|_{\Gamma^j}
\end{align}
Introducing the shorthand notation $\jump{\cdot}^j = (\cdot)^{j+} - (\cdot)^{j-}$, the above constraints take the form:
\begin{align}
   \jump{\mathbf{u}}^i &= \mathbf{0}, \quad \mathbf{x}\in\Gamma^i,
   \label{eq:continuityDisplacement} \\
   \jump{\bm{\sigma}}^j\cdot\mathbf{n}^j &= \mathbf{0}, \quad \mathbf{x}\in\Gamma^j.
   \label{eq:continuityTraction}
\end{align}
Equations~\eqref{eq:bulkEquations}--\eqref{eq:BCcontinuous} are equivalent to Equations~\eqref{eq:bulkEquationsSubdomains}--\eqref{eq:continuityTraction}.



\section{Mixed continuous formulation} \label{sec:mixedContinuous}

Equations~\eqref{eq:bulkEquationsSubdomains}--\eqref{eq:continuityTraction} are cast into the weak form for subsequent discretization.
First, we formally derive the equations and then choose the appropriate function spaces based on mechanical reasoning.

The strain energy of the deformable body and the work done by the surface tractions are the sum of the individual contributions on the grains, therefore the energy functional takes the form:   
\begin{equation}
   \Pi_b = \sum_{m \in \Isubdom}\frac{1}{2} \int\limits_{\Omega^m}\bm{\sigma}^m(\mathbf{u}^m):\bm{\varepsilon}^m(\mathbf{u}^m) \DIFF \Omega - \sum\limits_{i \in \INeu}\ \int\limits_{\Gamma^i_N}\mathbf{u}^{i+} \cdot \mathbf{t}_N^i \DIFF \Gamma.
   \label{eq:functionalBulk}
\end{equation}

Constraint equations are required to enforce the continuity equations~\eqref{eq:continuityDisplacement}--\eqref{eq:continuityTraction}.
During the discretization, we will see that the finite element nodes do not coincide with the interfaces or the boundaries.
Therefore, the continuity equations and the Dirichlet BCs are taken into account weakly.
An additional advantage of the weak imposition of Dirichlet boundary conditions is that the system matrix does not need to be modified.
The weak solution to \eqref{eq:bulkEquationsSubdomains}--\eqref{eq:continuityTraction} is found by minimizing the total potential energy \eqref{eq:functionalBulk} with respect to the weak constraints
\begin{equation}
   \Pi_c = \sum_{i \in \Iint}\ \int\limits_{\Gamma^i} \jump{\mathbf{u}}^i \cdot \bm{\lambda}^i \DIFF \Gamma + \sum_{i \in \IDir}\ \int\limits_{\Gamma^i_D} \mleft( \mathbf{u}^{i+} - \mathbf{u}^i_D \mright) \cdot \bm{\lambda}^i \DIFF \Gamma,
   \label{eq:weakConstraintFunctional}
\end{equation}
where $\bm{\lambda}^i$ are the vector-valued Lagrange multiplier functions.
The constrained optimization problem can be turned into a saddle-point formulation by constructing the Lagrangian $\Pi$ as
\begin{equation}
   \Pi = \Pi_b + \Pi_c
   \label{eq:functionalTotal}
\end{equation}
and taking its variation with respect to the two fields, i.e.\ the displacement and Lagrange multiplier fields.
The variational problem is then (cf.~\eqref{eq:abstractMixedFormulation}): \\
Find $\mathbf{u} \in V$ and $\bm{\lambda} \in \Lambda$ such that
\begin{subequations}
   \begin{alignat}{3}
      &a(\mathbf{u}, \mathbf{v}) + b(\bm{\lambda}, \mathbf{v}) &&= f(\mathbf{v}), \quad &&\forall \mathbf{v} \in V
      \label{eq:equilibriumEquation} \\
      &b(\mathbf{u}, \bm{\mu}) && = g(\bm{\mu}), \quad &&\forall {\bm{\mu}} \in \Lambda
      \label{eq:weakConstraint}
   \end{alignat}
\end{subequations}
As interfaces and Dirichlet boundaries are handled in the same way, with an abuse of notation, both will be referred to as interfaces.
In this way, $\Gamma \defeq \Gamma^i \cup \Gamma_D$ is introduced.
One can unify the interface and Dirichlet boundary conditions by noticing that boundaries are only surrounded by subdomains from one side.
Therefore, the displacement jump on an arbitrary internal interface $\Gamma^i$ or Dirichlet boundary $\Gamma_D$ is redefined as
\begin{equation*}
   \jump{\mathbf{u}}^i = 
   \begin{cases}
      \mathbf{u}^{i+} - \mathbf{u}^{i-} & \textrm{on } \Gamma^i \\
      \mathbf{u}^{i+} & \textrm{on } \Gamma^i_D
   \end{cases}
\end{equation*}
The symmetric bilinear and linear forms introduced in \eqref{eq:equilibriumEquation}--\eqref{eq:weakConstraint} are expressed by
\begin{gather}
   a(\mathbf{u}, \mathbf{v}) = \sum_{m \in \Isubdom}\ \int\limits_{\Omega^m} \bm{\varepsilon}^m(\mathbf{v}^m):\bm{\mathcal{C}}^m:\bm{\varepsilon}^m(\mathbf{u}^m) \DIFF \Omega \\
   b(\bm{\lambda}, \mathbf{v}) = \sum_{i \in \Iint \cup \IDir}\ \int\limits_{\Gamma^i \cup \Gamma^i_D} \jump{\mathbf{v}}^i \cdot \bm{\lambda}^i \DIFF \Gamma \\
   f(\mathbf{v}) = \sum\limits_{i \in \INeu}\ \int\limits_{\Gamma^i_N}\mathbf{v}^{i+} \cdot \mathbf{t}_N^i \DIFF \Gamma \\
   g(\bm{\mu}) = \sum\limits_{i \in \IDir}\ \int\limits_{\Gamma^i_D} \bm{\mu}^i \cdot \mathbf{u}^i_D \DIFF \Gamma
\end{gather}
It can be identified from the equilibrium equation~\eqref{eq:equilibriumEquation} that the Lagrange multiplier represents the traction vector on the interface:
\begin{equation}
   \bm{\sigma}^{i+}\cdot\mathbf{n}^{i+} = -\bm{\lambda}^i, \quad \bm{\sigma}^{i-}\cdot\mathbf{n}^{i-} = \bm{\lambda}^i,
   \label{eq:tractionLagrangeMult}
\end{equation}
where $\mathbf{n}^{i+} = -\mathbf{n}^{i-}$.
The corresponding derivation can be found in Appendix~\ref{sec:lagrangeMultiplierPhysicalMeaning}.

In the presence of cracks between the grains, the displacement field is discontinuous.
At a junction of interfaces (e.g.\ when two subdomains have multiple common interfaces), the normal vector to the interfaces changes direction in a discontinuous fashion.
Hence, the Lagrange multiplier field representing the tractions across interfaces must be discontinuous as well at such junction points according to \eqref{eq:tractionLagrangeMult}.
Therefore, the weak solution $(\mathbf{u},\bm{\lambda})\in(V,\Lambda)$ to the equations~\eqref{eq:bulkEquationsSubdomains}--\eqref{eq:continuityTraction} is sought in the broken Sobolev spaces
\begin{align}
   V &= \mleft[H^1(\Omega)\mright]^d = \bigtimes_{i \in \Isubdom} \mleft[H^1(\Omega^i)\mright]^d \\
   \Lambda &= \mleft[H^{-1/2}(\Gamma)\mright]^d = \bigtimes_{i \in \Iint \cup \IDir} \mleft[H^{-1/2}(\Gamma^i)\mright]^d
\end{align}
where $\bigtimes$ is the direct product and $d$ is the spatial dimension.
