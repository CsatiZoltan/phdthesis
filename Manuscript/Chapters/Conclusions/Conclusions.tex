\chapter{Conclusions} \label{ch:Conclusions}
\localtableofcontents



\section{Summary and global comments} \label{sec:conclusions}

The principal aim of the thesis was to enable efficient and flexible microcrack simulations in grain-based materials.
As a first step towards this goal, simplifying assumptions were made on the possible crack paths and on the constitutive model of the grains.
Based on these assumptions, a two-field variational formulation was developed to represent the displacement field on each grain, and the traction field on each interface and Dirichlet boundary segment.
The Lagrange multipliers provided the (weak) continuity of the displacement field on the whole assembly of grains.
This construction allows independent local calculations, prone to parallelization.
The discretization of this mixed formulation is achieved with \CutFEM, which makes the meshing trivial.
All the unknowns are defined at the nodes of the rectangular elements.
The displacement space is spanned by the bilinear shape functions of rectangular elements.
The Lagrange multiplier shape functions are constructed as a restriction of the nodal shape functions on the interfaces, with some connections among them.
Establishing the reduction algorithm for the Lagrange multipliers on Q1 elements is a novelty of our work.
Interfaces are allowed to cross the elements arbitrarily.
Numerical evidence was given of the stability of the resulting discrete mixed method.

The gradual degradation due to microcracking was then modelled by an extrinsic cohesive law, derived from a potential.
The potential was defined as a function of the cohesive opening, from which the traction-separation relation can be obtained, and an internal variable, which plays a role in formulating the evolution equations.
To avoid the problem of infinite terms in the compliance matrix upon the failure of a cohesive zone, an augmented Lagrangian method was used, modifying the original pure Lagrangian formulation.
Self-contact between the (partially) open crack lips was enforced explicitly.
It turned out that by discretizing this augmented Lagrangian method, most of the matrices can be precomputed, therefore the additional matrix assembly at each step is very cheap.
As the cohesive parameters are not uniform along the interfaces, the same damage increment at two different locations is not associated with the same amount of degradation in general.
To take this into account, a physically meaningful quantity, the energy dissipation, was introduced to drive the energy release.
The damage increment, present in the weak form, was then subsequently computed from the energy dissipation increment.
Algorithms shed light on the details of the overall solution process.

To harness the capabilities of the developed framework, three examples were considered.
The three-point bending test verified the numerical algorithms for mode I cracking.
A thorough analysis of the Brazilian test revealed the influence of the material and solution procedure parameters.
The number of grains in the Brazilian disk allowed a meaningful representation of realistic microstructures.
Our method enables the user to easily perform statistical analysis by generating random grain packings and running computations on them.
The third test was the uniaxial compression test, where even more grains were taken.
Qualitatively correct results were obtained both for the Brazilian test and for the uniaxial compression test, using crude meshes and large step sizes.

We consider the discretization method and the energy-based solution procedure for the propagation of multiple cracks as the main strength of the thesis.
The discretization itself, without including the damage, is worth highlighting due to its attractive properties:
\begin{enumerate*}[label=(\roman*)]
   \item no meshing difficulties,
   \item accurate computation of the traction vector,
   \item block diagonal subdomain stiffness matrices $\stiffnessmatrix^i$, and
   \item known null-space of $\stiffnessmatrix^i$, $\forall i\in I_s$.
\end{enumerate*}
The mesh-independence has the additional merit that the same background mesh can be used to run simulations on different grain distributions, which becomes useful for RVE computations or for parametric investigations as illustrated in Chapter~\ref{ch:Validation}. \\
The quasi-static stepping based on the energy dissipation has the advantage over the load-controlled solution procedures that a complex load-displacement response can be tracked without sophisticated path-following techniques.
It further increases the automated nature of the framework since the user does not have to experiment with the proper choice of the load-controlling parameters so that the nonlinear iteration procedure does not fail.
Its combination with an extrinsic cohesive zone formulation, parametrized by the energy dissipation, results in the automatic tracking of microcrack initiation and propagation.
Another nice property is that the damage update is separated from the solving of the mechanical system, making a segregated solution procedure possible.
Although the explicit update algorithm produces jagged load-displacement curves, this is mostly aesthetic as an envelope of the graph is easily recognizable. 
\skiponeline
The developed framework in its current stage has some deficiencies.
Although attempts were made in Section~\ref{sec:robustness} to achieve matrices with smaller condition number, a more efficient strategy is needed to be able to use iterative solvers.
Some prospects are recommended in item~\ref{item:preconditioning} of the next section.
The other main problem is that with the introduction of the augmented Lagrangian method, the penalty term destroys some good characteristics of the original saddle point system:
\begin{enumerate*}[label=(\roman*)]
   \item the choice of the penalty-like parameter $k$ is not dictated by the conditioning only, but its minimum value is determined by the cohesive material constants and the failure criterion; and
   \item the subdomain stiffness matrices $\stiffnessmatrix^i$ are complemented by interfacial terms, increasing the bandwidth and making the preconditioning more difficult.
\end{enumerate*}



\section{Future work} \label{sec:futureWork}

Now, that the initial ideas were realized in a mature computational framework, several further improvements are worth investigating in the future.
These ideas are classified as follows.
\begin{itemize}
   \item[\textbf{P.}] Additional \emph{physical} phenomena
   \begin{enumerate}[label=\textbf{P.\arabic*},ref=\textbf{P.\arabic*}]
      \item Frictional contact.
      Friction between surfaces of fractured rock may have a significant effect and could be included in the framework.
      \item Coupling with fluid flow in the microcracks for permeability computations.
      In the introductory section~\ref{sec:background+motivation}, we saw that microcracking-induced permeability change is important in practice.
      We plan to couple our damage model with low-velocity fluid flow in the channels formed by open cracks.
      Existing attempts, such as the ones in~\cite{Massart2014} and~\cite{Eijnden2016}, can benefit from the non-conforming discretization.
      \item Intrinsic cohesive zone model.
      Rock grains investigated in this thesis are closely packed, therefore it was reasonable to apply extrinsic cohesive zone.
      Our discretization was successfully tried on rubble masonry structures with non-Voronoi grains, but the resulting crack path was not satisfactory.
      This can be explained by the finite width and compliance of the mortar joints around the stones, requiring the use of an intrinsic cohesive zone model.
      \item Different angles of friction among the phases. \\
      For the Brazilian test and the compression test, the interfaces within a quartz cluster were set to higher strength and fracture energy values than for the other interfaces.
      It is worth checking what happens if the angle of friction is also changed on the interfaces.
      The different angles of friction on the interfaces might be obtained experimentally with a direct shear test machine.
      \item Determine the dilatancy.\\
      Due to its practical relevance, the characterization of dilatancy is on high priority on our list of future investigations.
	   We noted in the discription of the uniaxial compression test in Section~\ref{sec:uniaxialTest} that computing the dilatancy requires ``roller-type'' boundary conditions. See also \ref{item:moreBC}.
   \end{enumerate}
   \item[\textbf{M.}] \emph{Modelling} questions
   \begin{enumerate}[label=\textbf{M.\arabic*},ref=\textbf{M.\arabic*}]
      \item 3D modelling.
      Three spatial dimensions allow richer kinematics, essential to capture the permeability increase of fractured geomaterials before the occurrence of the full loss of the load-bearing capacity.
      The construction of the discrete Lagrange multiplier space remains the same in 3D, i.e.\ the cut edges play a role and not the intersected faces.
      The failure criterion and the damage update scheme also remain almost identical.
      However, it will be more difficult to cut the possibly concave polyhedral grains with the elements.
      The development to perform is therefore essentially of geometrical nature.
      \item Initial microcracks.
      Microcracks in rocks are results of either natural processes (tectonic movements, etc.) or man-made activities (excavation, boreholes, etc.)~\cite{Hamdi2015}.
      They act as stress-concentrators, significantly lowering the strength of rocks.
      Higher microcrack density causes the increase of permeability.
      The incorporation of pre-existing microcracks into our model can be easily done by prescribing non-zero damage values for some locations on some interfaces.
      Completely open cohesive cracks can be easily incorporated by setting $d = 1$ at the desired locations (e.g.\ along the interfaces lying between feldspar grains).
      This is an advantage of our dissipation-driven solution method over the classical load-controlled schemes in which it is not straighforward how to place the initial microcracks.
      \item Effect of grain concavity.
      Our method works without further development for concave grains too.
      It would be interesting to characterize the convexity of grains and investigate how macroscopic dilatancy is influenced by the uplifting effect of the shearing of convex-concave grain connectivities.
      \item Quantification of the microstructure. Use statistical methods, such as correlation functions, to quantify the clustering of the mineral species.
      \item Homogenization.
      Computational homogenization is typically an application our tool would excel at.
      With a single background mesh, a set of grain realizations for statistical studies can be generated to obtain macroscopic responses.
      \item Reduced-order modelling.
      The calculations could be made much faster -- though sacrificing accuracy -- by decreasing the number of unknowns in the bulk and on the interfaces.
      Fewer degrees of freedom for the bulk can be achieved by allowing fewer deformation modes for the elastic grains.
      The number of interfacial unknowns (Lagrange multipliers) might be lowered to three.
      These three scalar Lagrange multipliers would weakly enforce the three interface opening modes: opening in the normal direction, opening in the tangential direction and rotating around a point on the interface.
      Finally, one could try reducing all the unknowns onto the interfaces by static condensation.
      Reworking the current discretization framework to be compatible with model reduction is probably not easy and therefore it has low priority.
   \end{enumerate}
   \item[\textbf{N.}] \emph{Numerical} tasks
   \begin{enumerate}[label=\textbf{N.\arabic*},ref=\textbf{N.\arabic*}]
      \item \label{item:preconditioning} Preconditioning.
      An alternative to the attempts we tried to decrease the condition number is adding extra terms to the weak form which efficiently improves the conditioning, such as the ghost penalty method~\cite{Burman2010a}.
      General-purpose (black-box) preconditioners are often not powerful enough for saddle point matrices~\cite{Benzi2005}.
      With a deeper understanding of the continuous operators, one increases the chance to construct effective preconditioners~\cite{Hiptmair2006}.
      \item \label{item:moreBC} Support other types of boundary conditions.
      The use of periodic boundary conditions in computation homogenization allows one generating a smaller RVE compared to the case when displacement or traction boundary conditions are prescribed.
      The literature already contains solutions to weakly enforce periodicity~\cite{Svenning2016b}.
      Periodicity conditions can be implemented similarly to jump conditions, which are already contained in our method.
      Another improvement is to be able to prescribe only one component of the displacement vector.
      This corresponds to the mechanical model of roller support, used for example in the three-point bending test and the uniaxial compression test.
      A recent work discussing it in the non-conforming case is~\cite{Lu2019}.
      \item Higher-order Lagrange elements.
      In~\cite{Ferte2014} a stable Lagrange multiplier space was designed for conventional P2 triangles in \XFEM.
      One could try using higher-order elements with all degrees of freedom associated with the nodes, as it was done in GFEM~\cite{Duarte2000}.
      A nice generalization of the current discretization would be a $Q^k-Q^{k^{\star}}$ interpolation, i.e.\ $k$-th order polynomials for the displacements and reduced $k$-th order polynomials for the Lagrange multipliers -- all degrees of freedom placed at the nodes.
   \end{enumerate}
   \item[\textbf{I.}] \emph{Implementation}
   \begin{enumerate}[label=\textbf{I.\arabic*},ref=\textbf{I.\arabic*}]
      \item Distributed computing capabilities.
      Most operations are local, therefore little communication would be necessary among the distributed computing nodes.
      \item Local (non-conforming) mesh refinement.
      Currently, only a uniform mesh is supported.
      In certain examples (e.g.\ the three-point bending test), a local refinement would significantly reduce the memory and CPU resource requirements.
      The refinement could either be linked to an a posteriori error estimate or simply be purely geometric.
      For smooth boundary data (e.g.\ $L^2$-regularity), we expect a smooth displacement solution inside the grains and jumps across the interfaces.
      Therefore, refining the mesh only in the neighbourhood of the interfaces seems reasonable.
      It has the advantage that the mesh can be fixed in the beginning, so the matrix sizes do not change and therefore the matrices can be precomputed, as it is done in the current implementation.
      Note that a non-conforming quadtree-based refinement is sufficient in our framework.
      \item Performant, non-proprietary software.
      Using \MATLAB was convenient during the prototyping phase, but for reproducibility reason and to promote open science, free tools are welcome.
      The C\texttt{++} language is free, compiles to fast executables, and is widely used in scientific computing with a large number of existing libraries.
      \item \label{item:loadingMode} Be able to track the loading mode.
      To obtain more insight on why a given crack path develops, it is helpful if we know which part of the failure criterion is reached at a given location.
   \end{enumerate}
\end{itemize}
Some of these issues are part of ongoing work.
