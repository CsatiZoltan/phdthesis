\graphicspath{{Chapters/Introduction/Pictures/}}

\chapter{Introduction}  \label{ch:Introduction}
\localtableofcontents

\input{ChapterSummaries/Introduction-summary}



\section{Background and motivation} \label{sec:background+motivation}

Rocks are geological materials with complex behaviour due to many factors~\cite{Jing2003}.
Some rock types are porous and in certain applications, the thermo-or hydromechanical coupling cannot be neglected.
Rocks, in most cases, contain pre-existing fractures both at the macro- and at the micro-scale.
Moreover, they are elastically inhomogeneous and anisotropic.
In ``weak'' rocks, creep and relaxation may be relevant mechanisms~\cite{Jing2003}.
Also, rocks cannot be considered as virgin materials and due to tectonic movements, rock masses are subjected to a pre-existing stress state.
\begin{figure}[H]
   \centering
   \includegraphics[scale=0.8]{granite.png}
   \caption[Microstructure of a Lac du Bonnet granite]{Microstructure of a Lac du Bonnet granite. Mineral types: quartz (Qtz), plagioclase (Plag), potassium feldspar (Kfsp), biotite (Bt). Microcracking types: grain boundary (Grb), intragranular (Intr), transgranular (Tr). Picture taken from~\cite{Lim2012}.}
   \label{fig:granite}
\end{figure}

Making decisions in rock engineering problems is made more difficult due to size effects.
Commonly used rock mechanics tests typically operate on samples of sizes ranging from \SIrange{10}{500}{\milli\metre}.
By measuring their stress-strain response, macroscopic data (Young's modulus, Poisson's ratio, etc.) can be obtained.
However, it is known that the variation in the type and size of the constituents may influence the macroscopic properties.
It is important to take the size effect into account when making predictions on rock masses based on experiments.
The surface roughness of rock joints depends on the specimen size, leading to size effect as well.
As the hydromechanical properties also depend on the surface roughness, it is relevant to characterize it somehow.
The authors in~\cite{Fardin2001} carried out experiments on different sample sizes and realized that a minimum size is necessary so that the surface roughness in a rock mass can be characterized by measurements on laboratory samples.

Various failure mechanisms act in rocks.
Experiments showed that failure of granite in compression is governed by the following phenomena~\cite{Martin1994}:
\begin{enumerate*}[label=(\roman*)]
   \item closure of existing microcracks,
   \item linear elastic deformation,
   \item crack initiation and stable crack growth,
   \item unstable crack growth
   \item post-peak behaviour.
\end{enumerate*}
Microcracking leads to the progressive degradation of the macroscopic properties. 

We will consider here cohesive grain-based materials that are made up of tight packing of constituents without significant void parts. To separate these grains, non-zero traction is needed.
Working with such cohesive grain-based materials, including rocks, is important in many geomechanical applications.
Nuclear waste disposal in geological formations is nowadays considered.
Although the type of the neighbouring rock is selected to have low permeability, the excavation required for disposal alters the stress state in the surrounding of the underground opening~\cite{Eijnden2015}.
The stress redistribution in the excavation disturbed zone initiates microcracking in the rock.
The microcracks grow and nucleate to form larger scale fractures which act as preferential pathways for fluid.
The infiltration of nuclear waste to the surface through these channels has serious environmental consequences.
One of the major sources of material nonlinear behaviour in brittle rocks such as granite is microcracking~\cite{Souley2001}.
It is therefore of significant importance to study the permeability increase with respect to microcracking, which requires robust computational tools representing distributed cracking.

Several experimental techniques exist to measure the material parameters at the micro-scale and at the macro-scale.
The ultrasonic pulse velocity testing was used in~\cite{Vasconcelos2008a} to determine the tensile and compressive strengths and Young's modulus in granites.
Macroscopic laboratory tests such as the direct tension test, uniaxial and triaxial compression tests, or the Brazilian test can provide the global strength and Young's modulus based on the measured stress-strain curve.
The individual phases (mineral species) often have different material properties that may be difficult to evaluate.
Micro-indentation is a test procedure used to determine the micro-mechanical properties.
For heterogeneous materials, grid indentation is better suited~\cite{Mahabadi2012a}.
The fracture toughness can also be extracted from scratch test results.
Similarly to the characterization of the microstructure, numerical methods can also be used to investigate which microscopic material property values allow reproducing an experimentally observed probability distribution.

Newer modelling strategies and the improving computational capabilities made numerical simulations a useful technique, complementing experimental approaches to solve engineering problems in rock mechanics.
Due to the lack of available data and the complexity of rock behaviour, numerical simulations can often only provide qualitative results~\cite{Jing2003}.
They are however important because they can complement experiments that have their own difficulties:
\begin{itemize}
   \item long sample preparation time
   \item in heterogeneous materials, such as rock, the experimentally observed crack path substantially differs for each sample. It is therefore almost impossible to obtain the local characteristic material properties and the repeatability of the experiments is also an issue.
   \item the response of rock samples is often brittle. Hence the loading device must have a very careful displacement control or mixed control facility; suitable loading speed must be applied to achieve a quasi-static response and prevent abrupt failure~\cite{Vasconcelos2008}
   \item due to size effect, the properties measured on the sample may not represent the value of parameters in the large scale
\end{itemize}
Numerical methods allow the user developing a qualitative understanding of rock fracturing and allows quickly performing parametric studies and sensitivity analyses~\cite{Jing2003}.
Depending on a specific engineering problem at hand, only few of the previously mentioned mechanisms are considered in the developed model to manage the complexity.
Comparing the numerical and the experimental results is made harder by the fact that real-world materials always contain heterogeneities and existing microcracks.
If measurements are available only at the macro-scale, careful calibration of the fine-scale quantities is required in the numerical model.

Rocks are heterogeneous both at the macroscopic and at the microscopic scales.
Macroscopic heterogeneity comes from the different rock types and from the pre-existing fractures.
The micro-level heterogeneity is partly caused by pre-existing microcracks and the different mineral species with distinct material properties present in the rock~\cite{Mahabadi2012b}.
Accounting for such heterogeneities is crucial for an accurate description of such geomaterials.
The distribution and shape of the grains can be determined by $\mu$CT, followed by image processing to map the colours of the CT image to the aggregate types~\cite{Mahabadi2012a}.
If the exact microstructure of a particular sample is not considered important, computer-generated microstructures can be cheap alternatives.
They allow analysing the effect of specific features such as the size distribution, the convexity and the clustering of species.
Among the many microstructure generation frameworks, we mention the one in~\cite{Sonon2012}, which is able to generate a wide variety of, possibly densely packed, microstructures with great control.
For cohesive grain-based materials, Voronoi tessellations have long been applied~\cite{Alonso-Marroquin2005,Massart2012,Eijnden2016}.
Assigning different elastic properties ($E$, $\nu$) to the Voronoi cells was demonstrated to be able to capture the effect of elastic heterogeneity~\cite{Lan2010}.

The challenges of distributed microcracking modelling and the many important applications motivated the topic of this thesis.



\section{Objectives}

From the above introduction it becomes clear that the proper characterization of the microstructure and the microcracks is crucial to understand the failure of rock-like materials.
This thesis is concerned with the numerical modelling of diffuse or multiple microcracking in cohesive grain-based materials, with an emphasis on rocks such as granite.
By diffuse or multiple cracking, we mean a configuration in which cracks can propagate along multiple, a priori known, paths with potential related crack interactions, intersections and branching.
This is not to be confused with completely arbitrary, a priori unknown, microcracks modelled as damage growth in the bulk.
Although there exist numerous numerical techniques, some of them being reviewed in Section~\ref{sec:modellingTechniquesForRocks}, there is still room for new models and solution methods which are accurate, consisting of physical parameters, and that require little user intervention.
It is not our intention to replace the existing modelling methodologies, rather we wish to develop an efficient framework endowed with the following capabilities.
\begin{itemize} % To allow defining custom labels easily (https://tex.stackexchange.com/a/29851/119426)
   \item[\textbf{P.}] The \emph{physical model} must capture some relevant mechanisms in the failure process of rocks. It includes
   \begin{enumerate}[label=\textbf{P.\arabic*},ref=\textbf{P.\arabic*}] % https://tex.stackexchange.com/a/58714/119426
      \item \label{item:intergranularCracking} an intergranular crack propagation framework.
      In grain-based rock microstructure the preferential locations for microcrack formation are often the grain boundaries.
      This mechanism is called \emph{intergranular} cracking.
      Cracks going through the grain are referred to as \emph{transgranular} crack propagation.
      Several factors have an influence on the nature of the fracture of polycrystalline materials.
      When studying piezoelectric polycrystals, it was found that intergranular cracks are mainly present for small grains, while transgranular cracks emerge as the average grain size increases~\cite{Verhoosel2009}.
      Intergranular fracture is favourable in polycrystalline materials if the fracture toughness of the interfaces is less than that of the grains, as reported in~\cite{Nguyen2017}.
      The competition between inter-and transgranular fracture was thoroughly analysed in~\cite{Shabir2011}, where the authors identified that the magnitude of the fracture toughness of the grains and the grain boundaries have a significant impact whether intergranular or transgranular fracture is realized in quasi-brittle materials.
      \item a failure criterion which takes into account the compression for damage initiation.
      The compression-dependence of the mechanical response is significant in rocks, especially considering heterogeneity and deep configurations.
      It was observed that rocks do not only fail in tension but also in shear combined with compression.
      In rock mechanics, two of the most commonly used failure criteria are the Mohr-Coulomb and the Hoek-Brown~\cite{Hoek1980} criteria.
      Although well-suited for the shear and compression regime, these two empirical failure envelopes are not successful in fitting experimental results in tension.
      Therefore, both criteria were combined with cut-offs in the tension part of the stress state.
      \item \label{item:fractureProcessZone} a non-negligible fracture process zone.
      Initially, two types of approaches were used in rock mechanics: the strength of materials and the \emph{linear elastic fracture mechanics} (LEFM).
      The first one predicts the onset of damage initiation when a certain failure criterion is met, but does not model the post-peak response.
      LEFM can be used to investigate the conditions for the propagation of a crack, but the validity requirements of LEFM do not generally hold in rock masses.
      Indeed, cracks have a considerable characteristic length compared to the whole sample size.
      Furthermore,~\cite{Rubin1993} noticed that in a deep underground configuration at large confining pressure, the process zone is significant.
      This is why rocks should be modelled as quasi-brittle and not completely brittle materials.
      \item non-uniform fracture energy.
      Material parameters, including fracture properties, of rocks are not uniform in general.
      The dependence of the fracture energy on the compressive stress is indicated in Fig.~3.9. in~\cite{Goodman1989}.
      \item a continuum-based model.
      Discrete modelling methods are a natural choice for granular materials such as sand, but computations on initially cohesive rocks can efficiently be carried out by discretization methods that rely on a continuum description.
      The finite element method is widely used because of its mature mathematical basis, its accuracy, and the accumulated experience with it.
   \end{enumerate}
   \item[\textbf{M.}] The \emph{mechanical model} should
   \begin{enumerate}[label=\textbf{M.\arabic*},ref=\textbf{M.\arabic*}]
      \item \label{item:cohesiveZoneModel} give rise to simple equations, the discretization of which does not lead to a too costly numerical scheme.
      The grains of the rock are considered to undergo small deformation and rotation, and are therefore described by the equations of linear elasticity.
      All the complex phenomena are assumed to take place at the grain boundaries.
      Recalling what we wrote in~\ref{item:fractureProcessZone}, this modelling choice begs for the use of the cohesive zone concept.
      \item make use of the a priori known potential crack paths.
      Owing to criteria~\ref{item:intergranularCracking} and \ref{item:cohesiveZoneModel}, all the interfaces are assumed to be completely open.
      To suppress these openings before the crack actually appears there, constraint equations are needed, which are detailed in Section~\ref{sec:constraints}. In addition to the displacement field defined on the individual grains, the Lagrange multiplier field is also present, responsible for enforcing the constraints.
      \item couple well with the cohesive model.
      As the cohesive tractions of the cohesive zone formulation are related to the Lagrange multipliers (shown in Appendix~\ref{sec:lagrangeMultiplierPhysicalMeaning}), the Lagrange multipliers are not eliminated from the two-field formulation.
   \end{enumerate}
   \item[\textbf{D.}] The \emph{discretization method}
   \begin{enumerate}[label=\textbf{D.\arabic*},ref=\textbf{D.\arabic*}]
      \item must lead to a stable discrete mixed method for reliability and optimal rate of convergence.
      The background for stability is discussed in Sections~\ref{sec:LagrangeMultiplierMethod} and \ref{sec:stabilization}, and is applied to our problem in Section~\ref{sec:stableMixedFormulation}.
      \item should require no intervention from the user.
      Mesh generation for complicated grain configurations can be challenging for an automatic mesh generator.
      Human help in the meshing is tiring and time-consuming, especially when many grain realizations are tested in statistical studies.
      To decouple the function approximation from the mesh, the cut finite element method will be used.
      The idea behind mesh-independent methods is explained in Section~\ref{sec:enrichedFEM} and will be used for our problem in Sections~\ref{sec:approximations}--\ref{sec:discreteWeakForm} of Chapter~\ref{ch:ProblemFormulation}.
      By mesh-independent in this thesis, we point to a method that allows solving the equilibrium problem based on a non-conforming mesh, i.e.\ the mesh is decoupled from the domain.
      This is not to be confused with meshless methods, which only contain nodes without a mesh data structure.
      \item should be scalable.
      The computational geometric algorithms ideally have linear or close to linear complexity so that many grains can be handled in a reasonable amount of time.
      Large grain count is necessary to represent the real microstructure.
      Parallelization of the main steps of the complete framework is also desirable.
   \end{enumerate}
\end{itemize}



\section{Major contributions}

To complete the objectives worded above, the following main originalities are presented in this thesis.
\begin{itemize}
   \item A stable discrete Lagrange multiplier space is constructed for the displacement space discretized with bilinear quadrilateral elements. The inf-sup conditions are verified computationally to demonstrate that the constructed Lagrange multiplier space is stable.
   \item An unfitted structured background mesh is sufficient to treat complex heterogeneous aggregates of grains with arbitrary polygonal shapes.
   \item An explicit interfacial damage formulation is developed for crack propagation combining a Mohr-Coulomb criterion with tension cut-off and compression cap. Contact is handled in this formalism and different behaviours for tension and compression are taken into account with normal compressive stress-dependent mixed mode fracture energy.
   \item The above features and the careful implementation enable to conduct fast computations on highly heterogeneous structures.
   \item The use of the developed framework is illustrated based on indirect tension test-like configurations (e.g.\ Brazilian test) and on uniaxial compression tests, as frequently used on rock specimens in the literature.
\end{itemize}
