\chapter*{\centering Abstract} \label{ch:Abstract}

The microstructural modelling of multiple microcracking with competing crack paths and complex crack intersections, branching and interactions in heterogeneous quasi-brittle cohesive grain-based materials is a complex task with many far-reaching applications.
For instance, transfer properties in grain-based rocks are strongly impacted by microcracking, which requires its modelling.
The numerical modelling of such phenomena requires three main ingredients:
\begin{enumerate*}[label=(\roman*)]
   \item a proper account for the microstructural heterogeneous geometry representation, i.e.\ a description of grain boundaries,
   \item a discretization of the governing equations, allowing a kinematical description of crack propagation, and
   \item the formulation of an efficient crack propagation model.
\end{enumerate*}

In this thesis a flexible and general stable displacement--Lagrange multiplier mixed formulation is developed to model distributed cracking in cohesive grain-based materials in the framework of the cut finite element method using a non-conforming background mesh.
The displacement field is discretized on each grain separately, and the continuity of the displacement and traction fields across the interfaces between grains is enforced by Lagrange multipliers.
The design of the discrete Lagrange multiplier space is detailed for bilinear quadrangular elements with the potential presence of multiple interfaces/discontinuities within an element.
We give numerical evidence that the designed Lagrange multiplier space is stable and provide examples demonstrating the robustness of the method.
Relying on the stable discretization, a cohesive zone formulation equipped with a damage constitutive formulation expressed in terms of the traction is used to model the propagation of multiple cracks at the interfaces between grains.
The damage propagation is governed by an energetic formulation.
To prevent the crack faces from self-penetrating during unloading, a contact condition is enforced.
The solutions for the mechanical fields and the damage field are separately obtained and an explicit damage update algorithm allows using a non-iterative approach.
The damage formulation couples the normal and tangential failure modes, accounts for different tension and compression behaviours and takes into account the compression-dependent fracture energy in mixed mode.
The framework is applied to complex 2D problems inspired by indirect tension tests and compression tests on heterogeneous rock-like materials.
