# Changes made in the manuscript after submitting it to the jury



Only minor modifications were done in the scientific content. These are the following:

- The *Tools and state of the art* chapter abstract now follows the order that of the body of the chapter.
- The typo in Eq. (4.11) was corrected: $\mathbf{v}_h$ instead of $\mathbf{v}$ is correct in the discretized weak form.
- The sentence between Eq. (4.38) and (4.39) must be correctly written as "The assembly of the subdomain matrices $S_{k\ell}^i$ and the global matrix $\mathbf{S}$ is done exactly as for the stiffness matrices.".
- The incorrect "The mathematical crack tip marks the point which still has zero traction, while the physical crack tip separates the cohesive zone from the intact part of the body." sentence now reads "The mathematical crack tip separates the cohesive zone from the intact part of the body, while the physical crack tip marks the point which still has zero traction.".



## Changes in the language

- Synonyms were used in certain places.
- Some typos were caught.
- The subject pronoun and the verbs are now in agreement everywhere.
- Consistent usage of British English in the whole document.
- Comma after longer introductory phrases.
- Correct use of prepositions.



## Changes in the typesetting

- References (equation, figure, table, algorithm numbers and citations) do not start a line for better readability.
- Removed the box around the equations for consistency.
- As the cut element parts are not part of the mesh data structure (which contains the nodes, the elements, and their connectivity), the $"\in\mathcal{M}^i"$ and $"\in\mathcal{M}_{\Gamma}^i$" symbols were removed in lines 8 and 18 of Algorithm 5.1.
- In certain places with many figures, several whitespaces were present. They do not exist any more.
- The size of the load-displacement plots was decreased.
- Short caption titles in the *List of Figures* and in the *List of Tables*.
- Consistently abbreviated journal titles in the bibliography.
