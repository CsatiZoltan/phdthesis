\documentclass[12pt]{article}

\usepackage[left=3cm,right=3cm]{geometry} % change the margins
\usepackage{titlesec} % modify section titles
\usepackage{enumitem} % continue numbering in the subsequent enumeration environments
\usepackage{siunitx}
\usepackage{easyReview} % highlight changes made to the text

\titleformat*{\section}{\large\bfseries} % default is "\Large"
\newcommand{\skiponeline}{\\[1em]}

\title{Modifications incorporated in Zoltan Csati’s PhD manuscript based on the private defense exchanges}
\date{}

\begin{document}

\maketitle

\noindent The present document is produced to mention the modifications brought to the thesis manuscript to address the main comments as a result of the discussions held during the private PhD defense.
All corresponding modifications in the manuscript were incorporated in blue font (except for the figures).

\bigskip


\section*{Title and Chapter 1}

\begin{enumerate}
   \item \textbf{The title of the manuscript led to some discussion, particularly the wording ``diffuse cracking'' (instead of multiple cracking), as well as to a lower extent ``mesh-independent'' (``non-conforming mesh'' would have been probably more adapted) and ``grain-based''.} \\
   \textbf{Changing the title at this stage seems difficult in the French system (unlike at ULB), but the exact meaning of these wordings should be explicitly explained in the abstract and in the introduction of the manuscript to better underline them for readers.} \\
   \add{Additional remarks/sentences were included in the abstract (page~1) and in the \emph{Introduction} chapter (pages 5, 7 and 9) to make these points clear.}
\end{enumerate}



\section*{Chapter 2}

\begin{enumerate}[resume]
   \item \textbf{In allusion to DEM methods, the statement that DEM requires more parameters should be better substantiated (is it really true?), or should be softened (acknowledgement that DEM contains more ``numerical'' parameters?).} \\
   \add{More details on the parameters required by the DEM frameworks have been added in the \emph{Tools and state of the art} chapter on page~14.}
   \item \textbf{The exhaustive and rigorous review on non-conforming mesh methods are acknowledged by the committee. The review of such methods would be even more complete if these sections were accompanied by comments emphasizing the pros and cons of the different methods and their difficulties.} \\
   \add{On page~41, the paragraph dealing with the comparison of X-FEM and CutFEM is highlighted.}
\end{enumerate}



\section*{Chapters 3 and 4}

\begin{enumerate}[resume]
   \item \textbf{It is suggested to merge Chapters 3 and 4.} \\
   \add{Performed. The common title is now \emph{Problem formulation and discretization}. The cross-references were verified so that they point to the right locations.}
   \item \textbf{The exact purpose of Figure~4.12 and the corresponding explanations in the text are not sufficiently clear. Try to better explain the issue.} \\
   \add{The meaning of Fig.~4.12 of the original manuscript (now Fig.~4.14 in the revised manuscript) has been further detailed on pages 65 and 66.}
   \item \textbf{The Brazilian test is used as verification to illustrate linear elasticity. It would be useful to report the elastic normal stress distribution on the diameter aligned with the loading direction. This can be readily found in the literature. The evolution of this stress distribution could be reported for different mesh sizes to show how it converges to the expected distribution.} \\
   \textbf{Also, present better that this test is used in Chapter~4 for a verification purpose (not a validation purpose with respect to an experimental comparison).} \\
   \add{The plot for the normal traction distribution was added, comparing it with the exact solution. Its discussion is included in the thesis on page~74. The distinction between verification and validation is emphasized.}
   \item \textbf{Still on the Brazilian test, a Young modulus of \SI{40}{\mega\pascal} is used, which seems rather low for a rock. This is not a major issue as it is used with an analytical expression for verification, but a more physical value could be better used.} \\
   \add{It was a typo in the manuscript. In the computation $E=\SI{40}{\giga\pascal}$ was indeed used, which corresponds to the Young modulus of the biotite.}
   \item \textbf{In the use of the patch test, strain quantities would be easier to interpret than displacement fields (or comparison with strain energy quantities that you can derive directly from the stiffness matrix).} \\
   \add{The suggestion has been followed for an easier understanding by the reader. The strain energy was determined on page~59 and the virtual work done by the traction vector on the displacement jump vector is also checked on page~61.}
   \item \textbf{Correct typo on the $\mathbf{H}$ matrix (should be $\mathbf{S}$) on page~64.} \\
   \add{The typo has been corrected in the revised version.}
\end{enumerate}



\section*{Chapter 5}

\begin{enumerate}[resume]
   \item \textbf{It is possible to characterize angles of friction on a collection of grains at the mesoscale. An identification of the material parameters at this scale could therefore be envisioned. Try to add references to the literature on this topic.} \\
   \add{The prospect of considering different angles of friction on the interfaces is now mentioned in the new point \textbf{P.4} on page~125.}
   \item \textbf{The use of the compression cap is not physically motivated and not even phenomenological, but rather a numerical parameter that allows choosing values for the $k$ parameter}. \\
   \add{It is now highlighted in Section~4.7 that the use of the compression cap only serves numerical reasons, i.e.\ allowing the $k$ parameter to remain finite.} \\
   \textbf{In Chapter~6, a uniaxial compression case should be shown with different cap values to show that the effect of this cap remains limited on the obtained results.} \\
   \add{Simulations with different compressive strength values were carried out which are shown in Fig.~5.31 and are described on pages 116 and 117. To facilitate the interpretation of the cracking process, it is recommended to keep track of the loading modes, i.e.\ which part of the composite failure criterion is active when the critical traction values are reached at a given location. This additional development task was added to point \textbf{I.4} on page~127.}
   \item \textbf{Algorithm 5.1, lines 21-24 on page 88 should be checked. There is a tensor product notation used in these lines that should be avoided because used improperly.} \\
   \add{The Hadamard product was intended. As $\otimes$ denotes the tensor product, the notation was changed to $\circ$, being a common notation for the Hadamard (element-wise) product of matrices.}
\end{enumerate}



\section*{Chapter 6}

\begin{enumerate}[resume]
   \item \textbf{It would be nice to have an indication of the degree of dilatancy that is obtained on the overall samples on the uniaxial compression test (based on the volume change)}. \\
   \add{The dilatancy, i.e. the relative change of volume, could be computed as $\varepsilon_\textrm{vol} = \varepsilon_1 + 2\varepsilon_2$, where the assumption is made that the lateral principal strains are equal to the unique lateral strain computed in a plane strain simulation. In a perfectly uniaxial stress state, $\varepsilon_1$ and $\varepsilon_2$ could be determined by keeping track of the axial and lateral displacements, respectively. Due to our boundary conditions that do not allow rollers on the top and bottom boundaries, the stress state is neither uniaxial nor uniform, and the body deforms with barreling. With these boundary conditions, the elastic volume change is expected to be smaller than in the case when the lateral displacement on the horizontal boundaries is made possible (e.g. with Teflon sheets). More than that, the current boundary conditions do not allow parallel vertical crack openings, hence the volume of the opening cracks is also smaller than for the ``proper'' boundary conditions. The above thought experiment suggests that comparing the dilatancy with measurements will only be meaningful as soon as the suitable boundary conditions are implemented. This explanation was incorporated to the thesis on pages 114 and 115. Moreover, the investigation of dilatancy was also added to the list of future plans in point \textbf{P.5} on page~125 and referred to it in point \textbf{N.2} on page~126 as well.}
   \item \textbf{Uniaxial compression tests are usually led with a way to avoid the lateral confinement on top and bottom (Teflon sheet). The analysis of this effect could be added by running some simulations with boundary conditions allowing the lateral expansion at the top and bottom of the sample. One would expect to obtain more vertically oriented cracks. This is to be done if the current implementation allows.} \\
   \textbf{If not possible, this point should be better underlined in the problem statement (clearly mention that roller supports are not implemented) and in the interpretation of the results (also with respect to the dilatancy issue mentioned above), namely that the diagonal cracking may stem from the BCs.} \\
   \add{The implication of the lack of roller support is discussed in the previous point. To find a temporary solution applicable in the current code base, several attemps were made to address the boundary conditions. First, I tried to set increased stiffness for the loading platen corresponding to $\sigma_{yy}$. It did not help in avoiding the barrelling effect mentioned in the previous point. The problem could have been the soft response by the prescribed Neumann boundary condition. To compensate this softness, I increased the stiffness for $\sigma_{yy}$ and $\tau_{xy}$ too. It did not change the barrelling significantly. Next, I kept the increased stiffness for $\sigma_{yy}$ and $\tau_{xy}$, and set very low stiffness for $\sigma_{xx}$. As a result, the body could move horizontally at the top, but the Dirichlet BC still caused barrelling at the bottom. Furthermore, the top grain (loading platen) became too soft and produced wavy deformation. These results indicate that this important test example requires us to be able to prescribe displacement in one direction only. The cause of the diagonal failure pattern is highlighted on page~116.}
   \item \textbf{The type of plane state used for the 3 point bending test is not mentioned.} \\
   \add{The mention of a \emph{plane strain} assumption is now included.}
   \item \textbf{Several questions arose concerning the force or displacement control of the solution procedure. The fact that this is actually a dissipation driven procedure is quite non-standard for the reader. It would be useful to state it again in Chapter~6 in the presentation of the simulations, and to use this information when interpreting the results.} \\
   \add{The description for the first load-displacement curve with a snapback on page~106 now contains this reminder, helping the reader to interpret the results.}
   \item \textbf{Globally, the level of details in the description of the problems solved should be increased to allow any reader to reproduce the tests.} \\
   \add{I wrote down what program I used for generating the bounded Voronoi diagrams.}
   \item \textbf{Explain better the jagged shape of the response curves and produce (if it is possible) the corresponding envelopes.} \\
   \add{I could not find a simple way of adding a covering envelope, especially because the snapbacks render the circumscribed polygon concave, which excludes the use of a convex hull. The explicit nature of the computation, particularly for crude energy dissipation steps, can create spikes, which disallowed me to successfully use alpha shapes for finding the envelope. Nevertheless, the cause of the jaggedness is further explained in the thesis on page~106.}
   \item Better link the crack patterns to the microstructure when the elastically heterogeneous microstructures are considered. Try to link the cracking to the position of clusters (especially quartz that is the strongest phase) by superimposing the crack patterns on the microstructure. \\
   \add{For the final crack pattern, the new type of figures now replaced the two previous types, i.e. the one containing the microstructure and the one depicting the crack pattern. This new plot type shows better that the cracks prefer propagating near the boundaries of the quartz and within the softest phase, i.e. the feldspar. On the other hand, these new plots are worse at showing the multiple cracking in the sample. Therefore, the old plots were kept for representing the microcrack evolution.}
   \item \textbf{Explain how initial microcracking could be incorporated in the framework (more likely in the feldspar region).} \\
   \add{Completely open cohesive cracks can be easily incorporated by setting $d = 1$ at the desired locations (e.g. along the interfaces lying between feldspar grains). This is an advantage over the load-controlled solution methods in which it is not straighforward how to place the initial microcracks. \textbf{M.2} on page~126.}
   \item \textbf{Mention (at least in the perspectives) that a quantification of the microstructure could be a useful addition (\# of grains of the species, shapes of grains, quantification of clustering by correlation functions.} \\
   \add{This has been added to the future plans in point \textbf{M.4}.}
\end{enumerate}


\end{document}
