\ProvidesPackage{mythesis}

%%%%%%%%%% Required packages %%%%%%%%%%
% Miscellaneous
\usepackage[a-1b]{pdfx} % generate PDF/A-compliant document (recommended to be included as the first package)
\usepackage{totcount} % counter, needed to obtain correct appendix numbers
\usepackage{enumerate} % allow defining enumeration labels
\usepackage[inline]{enumitem} % allow inline enumerations
\usepackage[minNoteWidth=1.5cm,color=yellow,colorinlistoftodos,prependcaption,textsize=small,textwidth=2cm,shadow,splitting=weightedMedian]{luatodonotes}
\usepackage{easyReview} % highlight changes made to the text
\usepackage{fancyhdr}
\usepackage{tocloft} % control TOC
\usepackage{titletoc}
\usepackage[bookman]{quotchap} % chapter number and title flush right, do not write "Chapter"
\usepackage{etoc} % provides local TOC and full customization
\usepackage{chngcntr} % to change the section counter for the appendix
\usepackage{titlesec} % customize sections
\usepackage{siunitx} % consistent typesetting of numbers and their units
\usepackage[protrusion=true,expansion,verbose=true]{microtype} % micro-typographic extensions for beautiful typesetting; add the "draft" option to disable the package

% Maths
\usepackage{amsmath}
\usepackage{amsthm} % theorem-like environments
\usepackage{dsfont} % number sets
\usepackage{mathtools} % many packages need it
\usepackage[ISO]{diffcoeff} % simpler typing of derivatives
\usepackage{mleftright} % proper spacing around the \left and \right pairs

% Algorithms
\usepackage[chapter]{algorithm} % number algorithm chapter-wise
\usepackage{algpseudocode} % more functionalities for typesetting algorithms

% Graphics
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{tikzscale} % scale graphics while keeping the text size
\usepackage[framemethod=TikZ]{mdframed} % automatically split frames
\usepackage{subcaption} % sub-figures

% Spacing
\usepackage[top=2cm,bottom=2cm]{geometry} % customize document layout
\usepackage{float} % for the [H] option to enforce figure placement
\usepackage{setspace}
\usepackage{xspace} % automatically add space after a user-defined command if necessary
\usepackage{rotating} % insert wide figure in landscape mode
\usepackage{etoolbox} % low-level modification of Latex (I often used to change default spacing)

% Fonts
\usepackage{bm} % boldface Greek letters
\usepackage{textcomp} % Text Companion fonts; needed to get rid of the warnings of microtype when used with siunitx (https://tex.stackexchange.com/questions/373594/microtype-producing-dozens-of-unknown-slot-number-warnings#comment1150718_373607)

% Tables
\usepackage{booktabs} % nice tables
\usepackage{threeparttable} % allows properly placed footnotes in tables
\usepackage{multirow} % allows cells spanning more than one row in a table

% Symbols
\usepackage{amssymb}
\usepackage{stmaryrd} % fancy brackets

% Bibliography
\usepackage[numbers,square]{natbib}

% Hyperlinks
\usepackage{url}
\usepackage{hyperref} % cross-referencing


%%%%%%%% Package options %%%%%%%%
\usetikzlibrary{arrows.meta}
% Hyperlinking within the document
\hypersetup{linktoc=all, % everything is clickable in the TOC
   hidelinks,
   colorlinks=true, % do not have those ugly frames around the references
   citecolor=appropriateGreen, % color of the \cite commands
   linkcolor=appropriateRed, % color of the inner references (equation numbering, footnote, figures, etc)
   urlcolor=blue, % color of URL
   breaklinks=true, % break long URL
%   allcolors=black
}


%%%%%%%% PDF metadata %%%%%%%%
% Note that not all PDF viewers can display all the data
\begin{filecontents*}{\jobname.xmpdata}
	\Title{Mesh-independent modelling of diffuse cracking in cohesive grain-based materials}
	\Author{Zoltan Csati}
	\Language{en-GB}
	\Keywords{X-FEM\sep crack\sep cohesive zone}
	\Subject{In this thesis a flexible and general stable displacement--Lagrange multiplier mixed formulation is developed to model distributed cracking in cohesive grain-based materials in the framework of the cut finite element method using a non-conforming background mesh. The displacement field is discretized on each grain separately, and the continuity of the displacement and traction fields across the interfaces between grains is enforced by Lagrange multipliers.	The design of the discrete Lagrange multiplier space is detailed for bilinear quadrangular elements with the potential presence of multiple interfaces/discontinuities within an element.	We give numerical evidence that the designed Lagrange multiplier space is stable and provide examples demonstrating the robustness of the method.	Relying on the stable discretization, a cohesive zone formulation equipped with a damage constitutive formulation expressed in terms of the traction is used to model the propagation of multiple cracks at the interfaces between grains. The damage propagation is governed by an energetic formulation. To prevent the crack faces from self-penetrating during unloading, a contact condition is enforced. The solutions for the mechanical fields and the damage field are separately obtained and an explicit damage update algorithm allows using a non-iterative approach. The damage formulation couples the normal and tangential failure modes, accounts for different tension and compression behaviours and takes into account the compression-dependent fracture energy in mixed mode. The framework is applied to complex 2D problems inspired by indirect tension tests and compression tests on heterogeneous rock-like materials.}
	\Source{https://bitbucket.org/CsatiZoltan/phdthesis}
	\Copyright{This work is licensed under a Creative Commons Attribution 4.0 International License}
	\CopyrightURL{creativecommons.org/licenses/by/4.0}
	\Copyrighted{False}
\end{filecontents*}


%%%%%%%%%% Definitions %%%%%%%%%%

% Colors
\definecolor{appropriateGreen}{RGB}{0,120,0}
\definecolor{appropriateRed}{RGB}{165,0,0}
\definecolor{azureblue}{RGB}{50, 215, 255}

% Math
\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}
\newtheorem{remark}{Remark}
\newcommand{\defeq}{\mathrel{\mathop:}=} % defining equality
\newcommand{\suchthat}{\ | \ } % set-builder notation
\DeclareMathOperator*{\spa}{span} % linear span
\DeclareMathOperator*{\supp}{supp} % support of a function
\DeclareMathOperator*{\diam}{diam} % diameter of a set
\DeclareMathOperator*{\diag}{diag} % diagonal matrix
\DeclareMathOperator{\meas}{meas} % measure of a set
\newcommand*{\DIFF}{\mathop{}\!\mathrm{d}} % straight "d" letter for the differential sign
\newcommand{\jump}[1]{\mleft\llbracket {#1} \mright\rrbracket} % jump operator
\newcommand{\dualitypairing}[3][\empty]{\langle #2, #3 \rangle_{#1}} % duality pairing
\newcommand{\inner}[3][\empty]{(#2, #3)_{#1}} % inner product
\newcommand*{\image}{\mathrm{Im}} % image of an operator
\DeclareMathOperator*{\argmax}{argmax}
\DeclareMathOperator*{\argmin}{argmin}
\renewcommand{\slash}{/\penalty\exhyphenpenalty\hspace{0pt}} % http://tex.stackexchange.com/questions/121955/help-on-dealing-with-items-divided-with-slash

% Shortcuts to commonly used terms
\newcommand{\XFEM}{\mbox{X-FEM}\xspace} % mbox ensures that no hyphenation is done
%\hyphenation{CutFEM}
\newcommand{\CutFEM}{\mbox{CutFEM}\xspace}
\newcommand{\MATLAB}{\textsc{Matlab}\xspace} % the language MATLAB
\newcommand{\MATLABfunction}[1]{\texttt{#1}} % function or method name in MATLAB
\newcommand{\Isubdom}{I_s} % index set of the subdomains
\newcommand{\Iint}{I_\textrm{int}} % index set of the internal interfaces
\newcommand{\IDir}{I_D} % index set of the Dirichlet boundaries
\newcommand{\INeu}{I_N} % index set of the Neumann boundaries (with inhomogeneous BC)
\newcommand{\cohesiveLength}{\ell_{\textrm{ch}}}
\newcommand{\ft}{f_t} % tensile strength
\newcommand{\fc}{f_c} % compression strength
\newcommand{\GI}{G_\textrm{I}} % mode I fracture energy
\newcommand{\GII}{G_\textrm{II}} % mode II fracture energy
\newcommand{\wn}{\jump{u_n}} % normal opening
\newcommand{\wt}{\jump{u_t}} % tangential opening
\newcommand{\eq}[1]{#1_{\textrm{eq}}} % equivalent quantity
\newcommand{\teq}{\eq{t}} % equivalent traction
\newcommand{\teqc}{t_{\textrm{eq,c}}} % critical equivalent traction
\newcommand{\weq}{\eq{\jump{u}}} % equivalent opening
\newcommand{\weqc}{\jump{u}_{\textrm{eq,c}}} % critical equivalent opening
\newcommand{\stiffnessmatrix}{\mathbf{K}}
\newcommand{\couplingmatrix}{\mathbf{B}}
\newcommand{\penaltymatrix}{\mathbf{P}}
\newcommand{\penaltymatrixblock}{\mathbf{L}}
\newcommand{\damagematrix}{\mathbf{C}}
\newcommand{\grammatrix}{\mathbf{G}}
\newcommand{\zeromatrix}{\mathbf{0}}
\newcommand{\HoneNormmatrix}{\mathbf{S}}
\newcommand{\smallestNonzeroEigenvalue}{\beta_{h,\min}} % its square root is the inf-sup constant

% Graphics
\newcommand*\circled[1]{\tikz[baseline=(char.base)]{\node[shape=circle,draw,inner sep=0.5pt] (char) {#1};}} % proper alignment for circled text

% TODO notes (each for a for different purpose)
\newcommand{\addtodo}[1]{\todo[linecolor=appropriateGreen,backgroundcolor=appropriateGreen]{#1}}
\newcommand{\errortodo}[1]{\todo[linecolor=red,backgroundcolor=red]{#1}}
\newcommand{\uncertaintodo}[1]{\todo[linecolor=azureblue,backgroundcolor=azureblue]{#1}}
% Highlight the chosen word (https://tex.stackexchange.com/a/343465/119426)
\makeatletter
\if@todonotes@disabled
   \newcommand{\hlnote}[2]{#1}
\else
   \newcommand{\hlnote}[2]{\todo{#2}\texthl{#1}}
\fi
\makeatother

% Algorithms
% Redefine \Return to have a new line before it (http://tex.stackexchange.com/questions/238574/algorithm-return-statement-does-not-begin-on-new-line)
\let\oldReturn\Return
\renewcommand{\Return}{\State\oldReturn}
\newcommand{\vars}{\texttt} % for variables to be more visible
\algnewcommand{\LineComment}[1]{\State \(\triangleright\) #1} % for a comment stretching a whole line (http://tex.stackexchange.com/questions/74880/algorithmicx-package-comments-on-a-single-line)
\algnewcommand\AND{\textbf{AND}} % (http://tex.stackexchange.com/questions/50908/algorithms-and-boolean-operator-casuing-undefined-control-sequence-error)
\algnewcommand\NOT{\textbf{NOT}}
\algnewcommand\ALL{\textbf{ALL}}
\algnewcommand\ANY{\textbf{ANY}}

% Setting the headers, the default full-caps is ugly (https://tex.stackexchange.com/a/78553)
\renewcommand{\chaptermark}[1]{\markboth{#1}{}}
\renewcommand{\sectionmark}[1]{\markright{#1}}
\pagestyle{fancy}
\fancyhf{}
\fancyhead[LE,RO]{\thepage}
\fancyhead[LO]{\itshape\nouppercase{\rightmark}}
\fancyhead[RE]{\itshape\nouppercase{\leftmark}}
\renewcommand{\headrulewidth}{0pt}

% Consistent formatting of "List of algorithms" (https://tex.stackexchange.com/a/131585/119426)
\makeatletter
\begingroup
  \let\newcounter\@gobble
  \let\setcounter\@gobbletwo
  \globaldefs\@ne
  \let\c@loadepth\@ne
  \newlistof{algorithms}{loa}{\listalgorithmname}
\endgroup
\let\l@algorithm\l@algorithms
\makeatother

% Some extra space before and after the "List of figures/tables/algorithms" identifiers
\cftsetindents{fig}{1.5em}{3em}
\cftsetindents{tab}{1.5em}{3em}
\cftsetindents{algorithms}{1.5em}{3em}

% "Intentionally left blank" page (https://tex.stackexchange.com/a/1729/119426)
\newcommand*{\blankpage}{%
	\newpage
	\thispagestyle{empty}
	\vspace*{\fill}
	{\centering This page is intentionally left blank\par}
	\vspace{\fill}
}

% Misc
\interfootnotelinepenalty=10000 % prevent breaking of footnotes (https://tex.stackexchange.com/a/32210)
\newcommand{\sectionbreak}{\clearpage} % every section to a new page (https://tex.stackexchange.com/a/9505)
\AtBeginEnvironment{verbatim}{\microtypesetup{activate=false}} % disable protrusion in verbatim environments
\DisableLigatures{encoding = T1, family = tt*} % disable ligatures for typewriter T1 encoded fonts
\SetSymbolFont{stmry}{bold}{U}{stmry}{m}{n} % avoid warning "Font shape `U/stmry/b/n' undefined(Font) using `U/stmry/m/n' instead" with the jump operator (from https://latex.org/forum/viewtopic.php?t=26024)

\binoppenalty=\maxdimen % do not break line for inline math
\relpenalty=\maxdimen   % (https://tex.stackexchange.com/a/94397)

\newcommand{\skiponeline}{\\[2em]}

\setcounter{secnumdepth}{3} % also number the subsubsections (https://tex.stackexchange.com/a/42162/119426)

\counterwithout{footnote}{chapter} % continuous footnote numbering (https://tex.stackexchange.com/a/10449/119426)
