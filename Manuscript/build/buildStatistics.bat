## Create the statistics
cd ..

if not exist "statistics.html" (
   if not exist "mainSingleFile.tex" (
      cd build
      call "buildSingleTexFile"
      cd ..
   )
   texcount -stat -freq -macrostat -html -out=statistics.html -v3 mainSingleFile.tex
)
cd build