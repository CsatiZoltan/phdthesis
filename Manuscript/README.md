# Manuscript



## Contents

- [Thesis structure](#thesis-structure)
- [Compiling](#compiling)
- [Technical details](#technical-details)



## Thesis structure

```
Manuscript
|     README.md - this file
|     main.tex - main file, includes the chapters
|     main.pdf - compiled thesis
|     main.dep - lists the LaTeX packages and the external files that were used
|     mythesis.sty - style file, determines the look of the document and defines macros
|     statistics.html - autogenerated data about the manuscript
|--- Chapters - contains the individual chapters
|     |--- Abstract
|     |     |    Abstract.tex
|     |--- Introduction
|     |     |    Introduction.tex
|     |--- Tools
|     |     |    Tools.tex
|     |     |--- Pictures - various graphics files (.ipe, .pdf, .png, .tikz)
|     |     |     |    ...
|     |--- ProblemFormulation
|     |     |    ProblemFormulation.tex
|     |     |--- Pictures
|     |     |     |    ...
|     |--- Discretization
|     |     |    Discretization.tex
|     |     |--- Pictures
|     |     |     |    ...
|     |--- Cracking
|     |     |    Cracking.tex
|     |     |--- Pictures
|     |     |     |    ...
|     |--- Validation
|     |     |    Validation.tex
|     |     |--- Pictures
|     |     |     |    ...
|     |--- Conclusions
|     |     |    Conclusions.tex
|     |--- FutureWork
|     |     |    FutureWork.tex
|     |--- Appendices
|     |     |    ComputerCode.tex
|--- GlobalStructure
|     |    GlobalStructure.tex - detailed overview of what is covered in the thesis (later changed)
|     |    GlobalStructure.pdf - compiled output
|--- ChapterSummaries - half-page summary of each chapter
|     |    Introduction-summary.tex
|     |    Tools-summary.tex.tex
|     |    ProblemFormulation-summary.tex
|     |    Discretization-summary.tex
|     |    Cracking-summary.tex
|     |    Validation-summary.tex
|     |    all.tex - collection of all summaries
|     |    all.pdf - compiled output
```



## Compiling

First, obtain the source either by downloading it from [Bitbucket](http://bitbucket.org/CsatiZoltan/phdthesis/downloads) or by cloning it with
```
git clone https://CsatiZoltan@bitbucket.org/CsatiZoltan/phdthesis.git
```
Then

- compiling the global structure:

     ```
     pdflatex GlobalStructure.tex
     ```

- compiling the chapter summaries:

     ```
     pdflatex all.tex
     ```

- compiling the thesis itself:

     - if *arara* is installed (available in the package manager)
     
     ```
     arara main.tex
     ```
     
     - if *arara* is not installed or you do not want to use
     
     ```
     lualatex main.tex
     bibtex main.tex
     lualatex main.tex
     lualatex main.tex
     ```

The required packages are listed in *mythesis.sty*. For the compilation to be reproducible, the exact versions of the packages and the external files are listed in *main.dep*.



## Technical details

### Workflow

The repository containing my work related to the PhD is hosted on [Bitbucket](http://bitbucket.org/CsatiZoltan/phdthesis). Writing the thesis was an incremental process; each relevant step is accessible as a git commit. The list of steps and the modifications implemented in those steps can be followed on [this page](http://bitbucket.org/CsatiZoltan/phdthesis/commits). Ideas during the writing were logged and are available on the [Issues](http://bitbucket.org/CsatiZoltan/phdthesis/issues) page. The technical details about the typesetting of the thesis can be found in the [**Tools**/*Tools.md*](../Tools/Tools.md) file.

### Statistics

The manuscript is made up of 36094 words. Interested in other statistics? See *statistics.html*, which was generated by the *buildStatistics.bat* script located in the **build/** directory.


### Printing

To save paper, print two pdf pages on a single physical page. To achieve this, the margins are cropped using the *pdfcrop* program, shipped with several LaTeX distributions. The *cropThesis.bat* script in the **build/** directory calls *pdfcrop* with the proper options and creates the *main_cropped.pdf* file. Note that the hyperlinks are lost in this procedure.


### Single source file

The main .tex file has a modular structure, as shown in the [**Thesis structure**](#thesis-structure) section above. Some LaTeX tools (e.g. obtaining statistics) require the existence of a single source file. Use the *buildSingleTexFile.bat* script from **build/** to obtain *mainSingleFile.tex*. This is also a useful technique if you want to write a paper in a modular way but the publisher demands a single .tex file.

