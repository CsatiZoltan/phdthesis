The scope of this chapter is the construction of local finite element spaces for the two unknown fields: the displacement and the Lagrange multiplier, assessing the stability of the corresponding formulations, and creating robust algorithms.

The meshing of a complicated network of cracks is challenging even in 2D.
Therefore, the mesh generation and the function approximation are separated as much as possible in this work.
The method uses the cut finite element method (\CutFEM) to construct the local finite element spaces for the displacement field on each grain, which are then patched together to form the global finite element space.
The Lagrange multiplier space is constructed in a similar vein, with the exception that a reduction is introduced to decrease its dimension and therefore render the mixed method stable.
One advantage of CutFEM is its embarrassingly parallel nature.
Indeed, very little communication is needed between the subdomains, therefore the geometrical algorithms and the creation and assembly of the local finite element matrices and vectors can be done independently for each grain and interface.
The methodology introduced so far is general, but for simplicity and computational efficiency, a global Cartesian mesh is used as the background mesh.

The Lagrange multiplier space reduction algorithm is based on the relative position of the interfaces with respect to the mesh.
The stability of the resulting formulation is verified with the inf-sup test, and the patch test proves that the constant stress state can be reproduced by the designed method.

The ease of mesh generation comes at the price of mesh cutting requirements and more complicated quadrature.
Cutting the elements with the grains and with the interfaces is a polygon clipping problem in computational geometry.
Special care is needed to carry out these cutting operations robustly so as not to introduce kinematic inconsistency.
Two detailed algorithms are given to tackle this problem.
The first one modifies the geometry, while the second one leads to an optimization problem.