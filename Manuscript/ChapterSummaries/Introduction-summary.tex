The aim of this chapter is three-fold.
A short introduction to the challenges of rock mechanics modelling is given.
Then, with the application of the developed computational framework in mind, a series of objectives and major modelling steps are formulated, indicating which sections of the work elaborate on them.
Finally, the major contributions of the present work are emphasized.

This introductory chapter starts with the motivation of the thesis, explaining why it is relevant to simulate cracking in heterogeneous grain-based materials.
It is well-known that heterogeneous materials exhibit complex behaviours which are difficult to capture at the macroscopic level with phenomenological constitutive equations.
Therefore, the modelling is carried out at the meso-scale where the shape, size distribution and material properties of the constituents can be taken into account.
These microstructural properties are considered as parameters and the developed framework allows carrying out simulations on different realizations of aggregates of grains.
We focus our attention on quasi-static crack propagation in rocks as a practical application.
It turns out from the available literature that in many practically relevant cases, it is a good approximation to assume that the cracks propagate along grain boundaries (interfaces).

Restricting the possible crack paths to grain boundaries allows relatively cheap simulations.
For representing cracks, models can be based on the incorporation of degradation of those interfaces according to a failure criterion.
Alternatively, one can assume that the cracks are possibly present on all the interfaces from the beginning, but they are only activated at a position when a physical criterion predicts so.
This way, maintaining closed, a priori positioned would-be cracks leads to constraint equations.
Their mathematical treatment is classical in the continuous setting but can cause instability when the governing equations are discretized.

Experiments on rocks show that they can fail under compression if the accompanying shear is large enough.
Another experimental observation is that the shearing fracture energy increases with normal compression.
These phenomena, that have to be accounted for, make the modelling challenging.

In view of such challenges, the major contributions of this work include the formulation of a stable mixed cut finite element method with non-conforming Cartesian meshes of Q1 elements arbitrarily cut by interfaces, choosing the crack path for multiple competing cracks, and a cohesive model which takes into account the variable fracture energy.
The solution scheme allows an embarrassingly parallel implementation.
