\documentclass[a4paper,12pt]{article}

\usepackage[top=2cm,bottom=2cm,left=2cm,right=2cm]{geometry}
\usepackage{xcolor}
\usepackage{bm}
\usepackage[bookmarksdepth=3,bookmarksopenlevel=1,pdfdisplaydoctitle=true,pdftitle={Structure of the thesis},pdfauthor={Zoltan Csati},pdfkeywords={damage, X-FEM, Lagrange multiplier},pdfpagelayout={SinglePage},pdfnewwindow=true,verbose]{hyperref}

\setcounter{tocdepth}{2}

\definecolor{appropriateGreen}{RGB}{0,120,0}
\definecolor{appropriateRed}{RGB}{165,0,0}

\hypersetup{linktoc=all, % everything is clickable in the TOC
   colorlinks=true, % do not have those ugly frames around the references
   citecolor=appropriateGreen, % color of the \cite commands
   linkcolor=appropriateRed, % color of the inner references (equation numbering, footnote, figures, etc)
   %allcolors=black
}



\title{Structure of the thesis}
\date{}

%\doublespacing

\begin{document}


\maketitle

\tableofcontents


\section{Abstract}

I think this should be at most one-page long and should not contain any symbols, just plain English. It would consist of three paragraphs.
\begin{enumerate}
   \item Geomechanical background and why it is worth to deal with this topic
   \item Main components of the proposed framework
   \item  Validation on several examples
\end{enumerate}



\section{Introduction}

\subsection{Objectives}

Here I state the objectives of the thesis. I think I should give some modelling details, otherwise the description is too vague and no clear objectives can be worded. This part can
contain some references but still no equations and symbols. I do not enumerate all the modelling steps which were applied but the ones which are the most relevant. The main objectives, in this order (start from mechanics, through the continuous problem, to the discretized one), could be:
\begin{itemize}
   \item Quasi-static problem \\
   Write down why we do not focus on dynamical fracturing.
   \item Take into account cracking in presence of compression and shearing \\
   Refer to some experimental results which indicate that it is indeed important to consider cracking other than mode I.
   \item Assume a-priori given possible crack paths \\
   Refer to experimental papers to justify the intergranular cracking.
   \item Cohesive zone method to account for the nonlinearity in the fracture process zone \\
   Do not write here that it also helps in the Lagrange multiplier method, that is a lower level detail.
   \item Multiple competing cracks
   \item Heterogeneity in material and grain shape necessitates the investigation of the micro/meso scale $\rightarrow$ computational homogenization $\rightarrow$ need to create many realizations of grain.
   distributions $\rightarrow$ mesh-independent numerical technique
   \item Develop a fast computational tool for the automatic analysis of microcracking in cohesive grain-based materials
\end{itemize}

\subsection{Major contributions}

%Based on the objectives listed above, the reader knows the main requirements by now. We can list what we achieved. These are the following:
\begin{itemize}
   \item Quadrangular elements arbitrarily cut by possibly many interfaces in 2D
   \item Stable mixed method
   \item Interfacial damage model with variable fracture energy depending on the loading direction
   \item Create an efficient implementation of the framework\footnote{By efficient, I mean algorithmically. Actual speed cannot be compared with a code written in a compiled language.}
   \item Validation on practically relevant tests
\end{itemize}

\subsection{Outline}

Summary of what is done in each chapter.



\section{Tools and state of the art}

This chapter of the thesis gives an overview of the tools I rely on and summarizes the existing methods. We should discuss how much I need to write for each topic.

\subsection{Saddle point problems}

The literature of saddle point problems is huge. Here, I only concentrate on those aspects which appear in our formulation. I already have a detailed report from 2017.

\begin{itemize}
   \item Some examples leading to saddle point problem \\
   Discretization of PDEs: (Navier-)Stokes equation, mixed Laplacian, constrains with Lagrange multiplier
   \item Generalized saddle point problems \\
   Non-zero (2,2) block: often arises from stabilization $\rightarrow$ augmented Lagrange multiplier method
   \item Properties: ill-conditioning when different scales are present
   \item Solvability conditions
   \item Solution methods
   \begin{itemize}
      \item direct
      \item iterative: black-box preconditioners are not efficient for saddle point problems
   \end{itemize}
   \item inf-sup test as a necessary condition for the stability of discretized mixed methods
\end{itemize}

\subsection{Constraint equations}

Different techniques for enforcing constraint equations, with an emphasis on their discretization on non-matching meshes
\begin{itemize}
   \item Where they show up
   \begin{itemize}
      \item incompressibility (fluid and solid)
      \item transmission conditions (including Dirichlet BC)
      \item periodicity
      \item prescribed mean value
   \end{itemize}
   \item Variational form
   \begin{itemize}
      \item penalty
      \item Lagrange multiplier
      \begin{itemize}
         \item boundary and distributed Lag. mult. (see Glowinski)
         \item mortar methods (our method shows similarity to them), mentioning the relatively new dual Lagrange multiplier method
      \end{itemize}
      \item Nitsche (symmetric and unsymmetric versions, relation to Lag. mult.)
      \item bubble stabilization
      \item three-field formulations
   \end{itemize}
   \item Discretization
\end{itemize}
This is one of my favourite parts, so I read a lot of papers about it.

\subsection{Extended finite element method (X-FEM)} \label{sec:X-FEM}

\begin{itemize}
   \item Birth of X-FEM
   \begin{itemize}
      \item need came from meshless methods to impose essential BCs
      \item partition of unity method (PUM)
      \item PU-FEM/GFEM: local enrichment with handbook functions
      \item X-FEM: special functions for crack description: Heaviside + tip enrichment
   \end{itemize}
   \item Coupling with the level set method
   \item Extensions and applications
   \begin{itemize}
      \item 3D
      \item weak discontinuities
      \item branching and intersecting cracks
      \item cohesive cracks
      \item dynamic crack propagation
      \item contact
      \item image-based modelling
      \item use in (commercial) software
   \end{itemize}
   \item Technical issues
   \begin{itemize}
      \item blending elements
      \item quadrature: discontinuous (subdivision, moment fitting, Green's theorem) and singular functions
      \item geometrical vs topological enrichment
      \item vectorial enrichment
      \item adaptivity
      \item intrinsic X-FEM
      \item preconditioning (DOF removal, Cholesky, quasi-orthogonalization, etc.)
   \end{itemize}
   \item Related method: CutFEM/TraceFEM
   \begin{itemize}
      \item alternative names: fictitious/phantom node method
      \item simple geometrical meaning: overlapping elements
      \item comparison with X-FEM
      \begin{itemize}
         \item in the simple case, the X-FEM and the CutFEM bases span the same space
         \item element-centric as opposed to X-FEM which is node-centric
         \item fixed number of DOF per element
         \item no linear dependency for multiple discontinuities within one element
      \end{itemize}
      \item applications: surface PDEs (Laplace-Beltrami), domain decomposition
   \end{itemize}
\end{itemize}
I also have a lot of papers organized and marked with a highlighter, so collecting this will be easy.

\subsection{Crack propagation in heterogeneous rocks}

In October I already did a literature review about cracking in polycrystalline materials. That review mostly mentioned metallic crystals as an application. I can use parts of that review but I must concentrate on quasi-brittle materials in the \emph{Physics} section below. Here, I only enumerate the reviewed techniques in bullet points.

\subsubsection{Physics}

\begin{itemize}
   \item competition between intergranular and transgranular fracture
   \item cohesive length (formula for mixed mode ?)
   \item what influences the crack path
   \item effect of the morphology (convexity, heterogeneity, clustering of the grains)
   \item effect of the cohesive properties (strength, fracture energy)
\end{itemize}

\subsubsection{Models}

\begin{itemize}
   \item Spring networks
   \item Statistical methods
   \item Discrete crack models
   \begin{itemize}
      \item LEFM
      \item CZM
   \end{itemize}
   \item Smeared crack models/damage models
   \begin{itemize}
      \item various classical regularized damage models
      \item phase field approach
      \item TLS
   \end{itemize}
\end{itemize}
For each model, give the advantages and the drawbacks.

\subsubsection{Numerical methods}

\paragraph{FEM}

\begin{itemize}
   \item element removal
   \item zero-thickness interface elements
   \begin{itemize}
      \item along grain boundaries $\rightarrow$ intergranular cracking
      \item between each pair of bulk elements $\rightarrow$ transgranular cracking
   \end{itemize}
   \item grain boundaries of finite thickness
\end{itemize}

\paragraph{X-FEM}
\hspace{1em}

I am not sure whether to put X-FEM papers related to cracking here or to Section \ref{sec:X-FEM}, where the applications of X-FEM are detailed.

\paragraph{Other techniques}
\hspace{1em}

\begin{itemize}
   \item Voronoi cell finite element method (VCFEM)
   \item VEM
   \item BEM
   \item DEM
   \item FEM-DEM
\end{itemize}

I partly have literature review for the \emph{Physics} part as well, but I need to collect information specific to rock mechanics. Thierry's recommendations are the work of Mahabadi, Bésuelle, and publications from the following journals:
\begin{itemize}
   \item \href{https://agupubs.onlinelibrary.wiley.com/journal/19448007}{Geophysical Research Letters}
   \item \href{https://www.icevirtuallibrary.com/loi/jgeot}{G\'eotechnique}
   \item \href{https://ascelibrary.org/journal/ijgnai}{International Journal of Geomechanics}
   \item \href{https://onlinelibrary.wiley.com/journal/10969853}{International Journal for Numerical and Analytical Methods in Geomechanics}
   \item \href{https://www.journals.elsevier.com/computers-and-geosciences}{Computers \& Geosciences}
\end{itemize}



\section{Problem formulation}

In this chapter the strong and weak form of the linear elasticity equations are derived for multiple non-overlapping domains. The continuity of the displacement and traction fields are provided by Lagrange multipliers. 

\subsection{Definitions}

\begin{itemize}
   \item Grains
   \item Interfaces
   \item Boundary sets
   \item Orientation (outward unit normal to interfaces, CCW orientation for grains)
   \item Index sets
\end{itemize}
The article draft contains all of them.

\subsection{Governing equations}

Derivation of the strong form of the BVP.
\begin{itemize}
   \item Linear elasticity equations for the whole body as if there were no grains (equilibrium, constitutive, kinematic equations + BCs)
   \item The same equations as before, but now for each grain separately
   \item Continuity of the displacement and the traction fields
\end{itemize}

\subsection{Mixed continuous formulation}

\begin{itemize}
   \item Introduce the Lagrangian and derive the weak form
   \item Give the physical meaning of the Lagrange multiplier (i.e. the traction vector)
   \item Based on smoothness reasoning, show in which function spaces we search the solution for $\mathbf{u}$ and for $\bm{\lambda}$.
\end{itemize}


\section{Discretization}

\subsection{Discretizing the unknown fields}

\begin{itemize}
   \item Start with a Cartesian background mesh, mentioning the useful properties (diagonal Jacobian matrix due to the affine mapping, simple hierarchical refinement)
   \item Construct the cut meshes for the grains and for the interfaces (show in a figure)
   \item Define the \emph{local} FE spaces for the displacement field as it is standard in CutFEM
   \item Create the \emph{global} FE space for the displacement field by simply taking the direct sum of the local FE spaces.
   \item The Lagrange multiplier field is discretized in a similar vein as the displacement field, with the exception that not every node may hold independent Lagrange multipliers.
   \item Notice that the global Lagrange multiplier space is the direct sum of the local spaces in this case as well so that the discontinuity at a junction can be provided (refer to the next section)
   \item The Dirichlet BCs are imposed with the same technique as the internal interfaces.
   \item Emphasize the embarrassingly parallel nature of the mesh cutting and the creation of the local FE spaces
   \item As we have a basis now, the finite element matrices and vectors can be given.
   \item The assembly is quite straightforward:
   \begin{itemize}
      \item the DOF mapping for the displacement is standard
      \item the DOF mapping for the Lagrange multipliers take into account that several nodes may contain the same DOF
      \item knowing the DOF mappings and the cut element parts, the bulk and interface integrals can be computed on each element of the \emph{local} mesh
      \item The sparse local matrices are assembled to a sparse global matrix in case of a monolithic solution procedure
   \end{itemize}
\end{itemize}

\subsection{Constructing the discrete Lagrange multiplier space}

\begin{itemize}
   \item Start with definitions (cut edge, isolated node, intersection point, connectivity graph, etc.) and show them in a figure
   \item Cite some sources which report instability if all nodes hold independent multipliers. Remedy: enrich the displacement space or decrease the dimension of the Lagrange multiplier space. Justify our approach of decreasing the Lagrange multiplier unknowns.
   \item Define the rules to achieve this the and explain the role of them. Show on a sequence of figures how the DOF connectivity is created. We have two choices:
   \begin{itemize}
      \item the relative position of an interface to the quadrilateral element is only shown in figures
      \item in the first few months when I was in Nantes, I gave an alternative construction based on graph theory. It requires more notions to be defined but it is more elegant and the properties of the construction are easier to prove. I didn't see any article constructing the Lagrange multiplier space in this way. It uses a bit different algorithm than what is given in the paper of Nicolas and B. Wohlmuth. The essence of the method is the following. The cut edges form a graph and associating the Lagrange multipliers to the nodes is performed by partitioning this graph. In my work, I used METIS. If we choose this description, I have all the material for graph partitioning as I wrote a report about it.
   \end{itemize}
   Remark that the rules do not uniquely determine the Lagrange multiplier space
   \item Assess the stability by showing the inf-sup constant for decreasing mesh size (this is an important point, otherwise the reader may think that we will introduce the augmented Lagrangian method for coercivity -- which is not the case)
\end{itemize}

\subsection{Robustness}

\begin{itemize}
   \item raise the interest of a situation when an interface goes through a node or even coincides an edge
   \item cite some sources in the field of fictitious domain methods which do the following:
   \begin{itemize}
      \item do not consider this special case, arguing that this does not arise in practice
      \item modify the geometry (interface)
      \item move away the node
   \end{itemize}
   \item write down my solution which is detailed in the article draft
   \item my new algorithm to (not yet communicated, but I tested and it works in practice as well) handle this kind of situation
   \item generating an optimal background mesh which not only avoids these special cases, but also gives smaller condition number (Thierry knows about it but I haven't explained it to Nicolas yet). I already tested the idea in 1D few months ago, I can implement the 2D version in the code easily. The method is purely geometrical (PDE-independent and can be used for any convex element type) and I didn't meet this approach in fictitious domain papers.
\end{itemize}



\section{Diffuse microcracking in granular materials}

\subsection{Crack propagation along interfaces}

\begin{itemize}
   \item Naive approach: propagate crack by deactivating Lagrange multipliers
   \item Problem: crack then also extends to the neighbouring elements
   \item Use a cohesive zone model: non-negligible fracture process zone, smoother crack opening
   \item Instead of incrementing the load, use damage increments and deduce the load
\end{itemize}

\subsection{Mixed method for cohesive cracks}

Follow the work of Cazes.
\begin{itemize}
   \item extrinsic CZM
   \item introduce the cohesive law in its compliance form to the weak formulation
   \item redefine the Lagrange multiplier $\rightarrow$ it is no longer the traction vector
   \item introduce the contact condition 
\end{itemize}

\subsection{Failure criterion}

\begin{itemize}
   \item Rationale for using the Mohr-Coulomb failure criterion with tension and compression cut-off
   \item Equations of the initial damage surface, figure
\end{itemize}

\subsection{Damage-based cohesive zone formulation}

This subsection contains the most originality of my work. Nicolas asked why I use \emph{Damage-based} in the title. Since reformulation of the CZM with a damage variable is not the everyday CZM, I thought it should be pointed out. Since this part changed a lot since I wrote the second version of the draft of the paper, I will write a detailed workflow we could follow when writing this chapter.
\begin{enumerate}
   \item Start with a suitably defined Helmholtz free energy, which depends on the damage and on the displacement jump vector. We assume that the traction-separation relations are decoupled for tension and shear.
	\item A potential-based cohesive zone formulation is used. Cite the original PPR paper and its extension to extrinsic CZM to indicate that we are aware of potential-based CZMs. Mention that their reformulation with interfacial damage was done in the master thesis of Mulye at ECN. However, its use for the Mohr-Coulomb criterion and non-uniform fracture energy makes it practically impossible to use.
	\item Derive the traction vector and the energy release rate from the Helmholtz free energy. Express the traction vector with the augmented Lagrange multiplier.
   \item Replace the compliance term with the damage to obtain the final version of the weak form, used in the subsequent developments.
   \item Interpolate the damage with the same basis functions as the Lagrange multipliers.
   \item Entries of the linear and bilinear forms
   \item Change of basis for $\mathbf{k}$
   \item KKT conditions for the damage evolution
   \item Raise the issue of unloading as a result of stress redistribution. If Nicolas agrees with what I stated (i.e. the evolution laws inherently contain this information and the damage does not increase when there is unloading), then there is no need to use a fictitious cohesive law. Otherwise, I need to recast the current formulation.
   \item Computing the energy release rate for different combinations of the traction vector
   \item Assumptions on the mixed-mode fracture energy
   \item Computing the critical energy release rate for different combinations of the traction vector
\end{enumerate}

\subsection{Explicit update of the damage field}

\begin{itemize}
   \item why the damage variable is not a good measure of degradation
   \item introduce the dissipated energy as an indicator of degradation
   \item discretized evolution equations for the dissipated energy increment
   \item calculating the most critical location
   \item method to propagate damage at other locations as well
   \item computing the damage increment from the dissipated energy increment
   \item choosing the step size
\end{itemize}
   
\subsection{Outline of the solution scheme}

The global workflow, containing links to the corresponding sections where the ingredients were explained.



\section{Validation and applications}

\subsection{Three point bending test}

Good for demonstrating the method and a reference solution exists.

\subsection{Brazilian test}

In this more complex test, I need the following
\begin{itemize}
   \item few sentences about the Brazilian test as an indirect tension test used by experimentalists
   \item geometry, BC (determined by the experiment we want to compare our numerical solution with)
   \item parametric study on
   \begin{itemize}
      \item numerical parameters ($k$, step size, artificial diffusion, mesh size)
      \item microstructural properties (strength, fracture energy, etc.)
   \end{itemize}
\end{itemize}

\subsection{Computational homogenization}

Everything, except the periodicity enforcement is ready. However, I read about weakly imposing periodicity and the algorithm to do it in my code is ready on paper. I think this is a nice application which further demonstrates the robustness of the developed discretization framework. It would also give a future prospect of carrying out permeability computations, which was the main goal in the original PhD plan.



\section{Conclusions}

If every chapter contains the conclusions at their ends, perhaps there is no need for a distinct \emph{Conclusions} section.

\section{Future work}

\begin{itemize}
   \item Extension to 3D: mention that apart from the computational demand, it should be quite simple because
   \begin{itemize}
      \item the discrete Lagrange multiplier space construction remains the same, i.e. the cut edges play a role and not the intersected faces
      \item the failure criterion slightly changes: instead of one tangential component vector, one takes the square root of the two tangential components
      \item representation of 3D objects: hierarchical representation of polyhedra
      \item library for cutting general polyhedra: only found CGAL's Nef polyhedron
      \item direct solvers will be prohibitive
      \item the interpolation of the damage at the bulk nodes makes the damage evolution tracking very simple: nothing changes with respect to the 2D case
   \end{itemize}
\end{itemize}

% From https://tex.stackexchange.com/a/174628/119426
\appendix
\section*{Appendices}
\addcontentsline{toc}{section}{Appendices}
\renewcommand{\thesubsection}{\Alph{subsection}} % numbering with capital letters



\section{Choosing the parameter $k$}

\begin{itemize}
   \item Derive the minimum value of $k$ such that $y_c(d)$ is an increasing function.
   \item Give suggestions on the optimal value to achieve good condition number on the global matrix.
\end{itemize}



\section{Computer code}

The organization of this part of the appendix depends on how many pages I can write. I think the following should be mentioned under any circumstances:
\begin{itemize}
   \item requirements
   \begin{itemize}
      \item experimented with many different ideas throughout the PhD $\rightarrow$ general FE code, the emphasis is on the flexibility and not on the speed in the prototyping phase
      \item implementing CutFEM with Lagrange multipliers and its coupling with damage requires a lot of bookkeeping $\rightarrow$ create abstractions and hide as many details from the user as possible $\rightarrow$ OOP
      \item good scaling for multiple processors $\rightarrow$ parallelization
      \item the developed tool should be reusable $\rightarrow$ open-source, revision control
   \end{itemize}
   \item which existing libraries inspired me
   \item availability of the code on the website \ldots \\
   I favour open research and reproducibility.
   \item UML diagram of the classes
   \item main design principles
   \begin{itemize}
      \item separation of the building blocks (e.g. our application is available as a distinct module, leaving the rest of the FE code reusable in other types of applications)
      \item description of the FE part of the code
      \item description of the damage part of the code
   \end{itemize}
   \item main capabilities of the developed code
   \begin{itemize}
      \item non-overlapping domain decomposition method
      \item mathematical abstraction $\rightarrow$ easy to incorporate user-defined elements and degree of freedom mappings
      \item most part of the framework is parallel
      \begin{itemize}
         \item cutting the background mesh
         \item constructing the local FE spaces
         \item assembling the local (subdomain) matrices and vectors
         \item system solving (by calling MATLAB's multithreaded direct solvers)
         \item mention that these steps constitute (use bar charts) the major elapsed time; show it on a realistic example (many grains, moderately fine mesh) the portion of the run-times
         \item show the strong scaling
      \end{itemize}
   \end{itemize}
   \item efficiency
   \begin{itemize}
      \item slower as if it was written in a compiled language but provides great expressiveness
      \item made several optimizations (internal vectorization, precomputing quantities, sparse matrix from triplet, etc.)
      \item a table with columns (2 or 3) corresponding to the test cases (they can be Brazilian tests with different number of grains and mesh coarseness) and rows corresponding to the time spent on certain parts of the code (preprocessing, local assembly, global assembly, mass matrix update, damage update, solving)
   \end{itemize}
\end{itemize}


\end{document}