% Packages
\documentclass[a4,red]{mnposter}
\usepackage{multicol}
\usepackage{graphicx}
\usepackage[font=small, labelfont=bf]{caption}
\usepackage{subcaption} % sub-figures
\usepackage{float} % for the [H] option to enforce figure placement
\usepackage{amsmath, mathtools}
\usepackage{bm}
\usepackage{mleftright}
\usepackage{stmaryrd} % fancy brackets
\usepackage{enumitem}
\usepackage{parskip}
\usepackage{ragged2e}
\usepackage[numbers,square]{natbib}
\usepackage{hyperref}  


% Definitions
\setitemize{noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt}

\setlength{\bibsep}{0.0pt}

\columnsep     = 10pt % amount of white space between the columns
\columnseprule =  1pt % thickness of the black line between the columns

\definecolor{appropriateGreen}{RGB}{0,120,0}
\definecolor{appropriateRed}{RGB}{165,0,0}
\definecolor{azureblue}{RGB}{50, 215, 255}

\newcommand*{\DIFF}{\mathop{}\!\mathrm{d}} % straight "d" letter for the differential sign
\newcommand{\jump}[1]{\mleft\llbracket {#1} \mright\rrbracket} % jump operator
\newcommand{\suchthat}{\ | \ } % set-builder notation
\newcommand{\defeq}{\mathrel{\mathop:}=} % defining equality
\DeclareMathOperator*{\spa}{span} % linear span
\newcommand{\Isubdom}{I_s} % index set of the subdomains
\newcommand{\Iint}{I_\textrm{int}} % index set of the internal interfaces
\newcommand{\IDir}{I_D} % index set of the Dirichlet boundaries
\newcommand{\INeu}{I_N} % index set of the Neumann boundaries (with inhomogeneous BC)

\hypersetup{linktoc=all, % everything is clickable in the TOC
   hidelinks,
   colorlinks=true, % do not have those ugly frames around the references
   citecolor=appropriateGreen, % color of the \cite commands
   linkcolor=appropriateRed, % color of the inner references (equation numbering, footnote, figures, etc)
   urlcolor=blue, % color of URL
   breaklinks=true, % break long URL
}

\graphicspath{{Figures/}}

% Poster header
\title{\Huge{Mesh-independent modelling of diffuse cracking in cohesive grain-based materials}}
\author{\vspace{-0.25cm}\underline{Z. Csati$^{1,2}$}, N. Mo\"es$^{2}$, and T. J. Massart$^{1}$ \\
   $^{1}$\! Universit\'e libre de Bruxelles \quad $^{2}$\! \'Ecole Centrale de Nantes}



% Main part (text for the poster)
\begin{document}

\section{Motivation}

\begin{itemize}
   \item safety in nuclear waste deposit
   \item complement expensive, slow or complicated experiments
   \item automated workflow in analysing diffuse cracking
\end{itemize}

\begin{figure}[H]
   \centering
   \includegraphics[scale=0.4]{granite.png}
   \caption[Microstructure of a Lac du Bonnet granite]{Microstructure of a Lac du Bonnet granite. Mineral types: quartz (Qtz), plagioclase (Plag), potassium feldspar (Kfsp), biotite (Bt). Microcracking types: grain boundary (Grb), intragranular (Intr), transgranular (Tr). Picture taken from \cite{Lim2012}.}
   \label{fig:granite}
\end{figure}



\section{Physical considerations}

\begin{itemize}
   \item microcracking is the main failure mechanism in rocks
   \item intergranular crack propagation
   \item larger shearing resistance in the presence of compression
   \item non-negligible fracture process zone
   \item non-uniform fracture energy
\end{itemize}



\section{Model}

\begin{itemize}
   \item a-priori known crack paths
   \item continuum-based model $\rightarrow$ finite element method
   \item cohesive zone model (CZM)
\end{itemize}



\section{Continuous formulation}

\begin{itemize}
   \item linear elasticity equations for the grains
   \item displacement field independent on all grains \cite{Simone2006}
   \item continuity of displacements and tractions across the interfaces
   \item include the cohesive tractions in compliance form \cite{Cazes2012}
   \item avoid self-contact
   \item reformulation of the CZM with an internal variable \cite{Le2018}
\end{itemize}
\scalebox{0.9}{
\begin{equation*}
   \begin{cases}
      \textrm{Find } (\mathbf{u},\bm{\zeta}) \in V \times \Lambda \textrm{ such that } \forall (\mathbf{v},\bm{\eta}) \in V \times \Lambda \\
      \begin{gathered}
         \int\limits_{\Omega}\bm{\varepsilon}(\mathbf{v}):\bm{\mathcal{C}}:\bm{\varepsilon}(\mathbf{u}) \DIFF \Omega + \int\limits_{\Gamma} \jump{\mathbf{v}}\cdot \mleft( \bm{\zeta}-\mathbf{k}\cdot\jump{\mathbf{u}} \mright) \DIFF \Gamma = \int\limits_{\Gamma_N} \mathbf{v}\cdot\mathbf{t}_N \DIFF \Gamma - \int\limits_{\Gamma_D} \mathbf{v}\cdot\mathbf{k}\cdot\mathbf{u}_D \DIFF \Gamma, \\
         \int\limits_{\Gamma} \bm{\eta}\cdot \mleft( \jump{\mathbf{u}} - \chi d \mathbf{k}^{-1}\cdot\bm{\zeta} \mright) \DIFF \Gamma = \int\limits_{\Gamma_D} \bm{\eta}\cdot\mathbf{u}_D \DIFF \Gamma.
      \end{gathered}
   \end{cases}
   \label{eq:weakFormCohesiveContactDamage}
\end{equation*}
}

\vspace{10em}


\section{Discretization}

\begin{itemize}
   \item global approximation: $V_h = \bigoplus\limits_{\mathclap{i \in \Isubdom}} V_h^i$, \quad $\Lambda_h = \bigoplus\limits_{\mathclap{i \in \Iint \cup \IDir}} \Lambda_h^i$
   \item local approximations:
\begin{figure}[H]
   \centering
   \begin{subfigure}[b]{0.22\textwidth}
      \centering 
      \scalebox{0.7}{$V_h^i \defeq \spa \{ \hat{\bm{\psi}}^i_j \suchthat j\in \mathcal{M}^i \}$, $\hat{\bm{\psi}}^i_j \defeq
         \begin{cases}
            \bm{\psi}^i_j & \textrm{on } \Omega^i \\
            0             & \textrm{on } \Omega\setminus\Omega^i 
      \end{cases}$} \\
      \includegraphics[scale=0.7]{traceBulkMesh.pdf}
      \caption{Submeshes for the bulk}
      \label{fig:submeshBulk}
   \end{subfigure}
   \hfill
   \begin{subfigure}[b]{0.22\textwidth}
      \centering
      \scalebox{0.7}{$\Lambda_h^i = \spa \mleft\{ \tilde{\bm{\psi}}^i_j \suchthat j \in \tilde{\mathcal{M}}^i_{\Gamma} \mright\}$, $\tilde{\bm{\psi}}^i_j = \sum\limits_{k \in \mathcal{M}^i_{\Gamma}} \alpha_{jk}\bm{\psi}_k|_{\Gamma^i}$} \\
      \includegraphics[scale=0.7]{traceSurfaceMesh.pdf}
      \caption{Submesh for the surface}
      \label{fig:submeshSurface}
   \end{subfigure}
   \caption{Constructing the local approximation spaces}
   \label{fig:submeshes}
\end{figure}
\end{itemize}



\section{Stability}

\justify
The discretization is tested on a grain configuration subjected to uniaxial tension. Figure~2 shows that the constant stress state can be obtained, hence optimal order of convergence is expected. The inf-sup tests are evaluated on more and more refined meshes to ascertain about the stability of the mixed formulation (see Fig.~3).
\begin{figure}[H]
   \centering
   \begin{minipage}[b]{0.2\textwidth}
      \centering
      \begin{subfigure}[t]{0.45\textwidth}
         \centering
         \includegraphics[scale=0.32]{tensiontestHorizontalDisplacement.png}
         \caption{$u_x$}
         \label{fig:tensiontestHorizontalDisplacement}
      \end{subfigure}
      \hfill
      \begin{subfigure}[t]{0.45\textwidth}
         \centering
         \includegraphics[scale=0.29]{tensiontestVerticalDisplacement.png}
         \caption{$u_y$}
         \label{fig:tensiontestVerticalDisplacement}
      \end{subfigure}
      \label{fig:tensionTest}
      \caption{Tension test}
   \end{minipage}
   \hfill
   \begin{minipage}[b]{0.26\textwidth}
      \begin{subfigure}[t]{0.45\textwidth}
         \centering
         \includegraphics[scale=0.5]{LBBConstant.pdf}
         \caption{LBB constant \cite{Chapelle1993}}
         \label{fig:LBBConstant}
      \end{subfigure}
      \hfill
      \begin{subfigure}[t]{0.45\textwidth}
         \centering
         \includegraphics[scale=0.5]{coercivityConstant.pdf}
         \caption{Coercivity constant \cite{Arnold2009}}
         \label{fig:coercivityConstant}
      \end{subfigure}
      \label{fig:stabilityTest}
      \caption{Stability test}
   \end{minipage}
\end{figure}



\section{Application to the Brazilian test}

\begin{figure}[H]
   \centering
   \begin{subfigure}[b]{0.09\textwidth}
      \centering
      \includegraphics[scale=0.53]{BrazilianRealization2.pdf}
      \caption{Realization}
      \label{fig:BrazilianClusteredHeterogeneity1}
   \end{subfigure}
   \hfill
   \begin{subfigure}[b]{0.09\textwidth}
      \centering 
      \includegraphics[scale=0.07]{failure1000.png}
      \caption{Snapshot A}
      \label{fig:BrazilianHeterogeneous7_evolution_A}
   \end{subfigure}
   \hfill
   \begin{subfigure}[b]{0.09\textwidth}
      \centering
      \includegraphics[scale=0.07]{failure2000.png}
      \caption{Snapshot B}
      \label{fig:BrazilianHeterogeneous7_evolution_B}
   \end{subfigure}
   \hfill
   \begin{subfigure}[b]{0.09\textwidth}
      \centering 
      \includegraphics[scale=0.07]{failure2800.png}
      \caption{Snapshot C}
      \label{fig:BrazilianHeterogeneous7_evolution_C}
   \end{subfigure}
   \hfill
   \begin{subfigure}[b]{0.09\textwidth}
      \centering 
      \includegraphics[scale=0.07]{failure3500.png}
      \caption{Final crack path}
      \label{fig:BrazilianHeterogeneous7_evolution_D}
   \end{subfigure}
   \caption{Progressive crack propagation in a heterogeneous sample}
   \label{fig:BrazilianHeterogeneous7_evolution}
\end{figure}


% References
\scriptsize
\bibliographystyle{../Bibliography/myabbrvnat-clickable}
\bibliography{../Bibliography/Crack,../Bibliography/XFEM,../Bibliography/SaddlePoint,../Bibliography/Rock}

\end{document}
