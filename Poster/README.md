The binary is available, but if you want to compile, do

```bash
pdflatex -interaction=nonstopmode BATirPoster
bibtex BATirPoster
pdflatex -interaction=nonstopmode BATirPoster
pdflatex -interaction=nonstopmode BATirPoster
```

There is a bug in the poster template I used, which has no visible effect but throws some errors. That is why *nonstopmode* is included above.