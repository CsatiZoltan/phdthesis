# Tools

Utilities used for the manuscript and for the presentation



## Contents

- [Latex source editor](#latex-source-editor)
- [Bibliography manager](#bibliography-manager)
- [Build system](#build-system)
- [Version control](#version-control)
- [Spelling](#spelling)
- [Static analyzers](#static-analysers)
- [Drawing](#drawing)
- [Clickable references](#clickable-references)
- [Decreasing the file size](#decreasing-the-file-size)
- [Single source file](#single-source-file)
- [Statistics](#statistics)
- [Cropping](#cropping)



## Latex source editor

The powerful, cross-platform [TeXstudio](https://www.texstudio.org/) editor was used for a number of reasons:

- open-source
- cross-platform (sometimes I work on Linux, sometimes on Windows)
- actively maintained
- user-defined commands (I associated a key to invoke *arara*)
- internal, detachable viewer (proved really useful when using two screens)
- directives (I prescribed to automatically use the *lualatex* compiler)
- automatically finds the root in case of a hierarchy of files (I didn't need to navigate to *main.tex*, I just pressed `F5` whichever subfile I was in, and the compilation automatically started on *main.tex*)
- automatic completion (not only environment names, but also commands with multiple options)
- built-in spellchecker
- [integrates with LanguageTool](http://wiki.languagetool.org/checking-la-tex-with-languagetool#toc2) and makes it recognize Latex markup
- ... and many other utilities which made typing frustration-free (integration with `texdox`, project source tree, etc.)



## Bibliography manager

I used the, in my opinion, best cross-platform (Java-based) Bibtex editor, [JabRef](http://www.jabref.org/) because it can
- automatically fetch Bibtex entry from the web based on a given DOI, ISBN, etc. (huge time-saver)
-  copy-paste entries between different .bib files
- directly push reference to TeXstudio if it is open
and it is
- integrated with [Mr. DLib](http://mr-dlib.org/information-for-users/information-about-mr-dlib-for-jabref-users/) (it happened that it pointed me to interesting articles)
- actively maintained
moreover, it has
- numerous warning indicators (missing compulsory fields, existing key, existing Bibtex entry, surround capital letters by { }, etc.)
- large (and extendable) database for journal abbreviations (saves time and more consistent than giving manually)
- a lot of other capabilities (define groups, filtering entries, clicking to the entry opens the online version on the publisher's website) I use every now and then



## Build system

Among the several available tools, I am (a bit) familiar with `latexmake` and `arara`. The former one automatically determines which compilers and how many times must be called, while `arara` allows much greater control. However, the directives for `arara` must be supplied by the user. As `arara` offers too much for the scope of this guide, consult with the [official site](https://github.com/cereda/arara) for details. Later versions of `arara` are shipped with the Latex distributions, so no need for manual install. The compilation of my thesis can easily be done by simply issuing `arara main.tex`.



## Version control

As Latex markup is pure text, it can be added to a version control system. As [recommended](https://stackoverflow.com/questions/6188780/git-latex-workflow), each sentence in the source is in its own line to make versioning easy. I used [git](https://git-scm.com/) for distributed version control, and [Smartgit](https://www.syntevo.com/smartgit/) as an editor. Smartgit
- is cross-platform
- has a powerful built-in diff tool
- is easy to use
- supports advanced git concepts (git LFS, git-flow, save stashes file-wise, git submodules)
- ships with a git shell
- is actively maintained

For online hosting, I chose [Bitbucket](https://bitbucket.org/) because it offers free private repositories. Today, I would probably choose [Gitlab](https://gitlab.com/) as that one has more features. Migrating between hosting services is possible.



## Spelling

Even native speakers can benefit from spell checkers. I used a number of tools to improve the quality of my thesis.

### LanguageTool

TeXstudio has built-in support for LanguageTool.

### Grammarly

There is a free version available. Even more sophisticated than LanguageTool but only knows in English (although with different dialects).



## Static analysers

Although not a natural language, tools are available to check the quality of the Latex code

### checkcites

`checkcites` checks for unused or undefined references. Useful when finalizing your work.

```bash
checkcites -u main // unused references
checkcites -U main // undefined references
checkcites -b <backend> main // backend; default: bibtex, you can use biber with - b biber
checkcites -a main // all unused and undefined references
```

### nag

This is a Latex package to mark obsolete Latex typing habits (e.g. using `\bf` instead of `\textbf`). I included it as the first package using

```latex
\RequirePackage[l2tabu, orthodox, experimental]{nag}
```

### lacheck

Simply type

```bash
lacheck main
```

to the terminal (or optionally redirect to a text file). It automatically follows the included files, so it is sufficient to execute it on the top-level source file.

### ChkTeX

Semantic checker. Type `chktex -h` for a short help. To be able to analyse the whole thesis, execute it on the [single-file version](#single-source-file). I used it with the following settings:

```bash
chktex -v1 -q mainSingleFile.tex -o chktex.txt
```

### TeXstudio's built-in analyser

The TeXstudio editor indicates some inconsistencies, e.g. brace mismatch, missing or multiply defined references and citations, missing packages, etc.



## Drawing

Except for one screenshot, all the figures are mine and were created by one of the following three programs
- two simple figures were directly drawn in [TikZ](https://ctan.org/pkg/pgf)
- sketches were made with [ipe](https://ipe.otfried.org/), as it
  - creates vector graphics (a must for quality figures)
  - supports arbitrary Latex markup (also vital for me)
    and it is
  - open-source
  - cross-platform
  - easy to use
- plots were done in MATLAB and converted to pgfplots/TikZ by [matlab2tikz](https://github.com/matlab2tikz/matlab2tikz). That program turned out to be extremely useful because I could create the desired global look of the figure in MATLAB (e.g. double logarithmic axis) and it was converted to a .tex file I could fine-tune later. End result: beautiful plots (see e.g. [this one](../Manuscript/Chapters/Validation/Pictures/threePointBendingLoadDisplacement.pdf)) with label sizes agreeing with the text size of the document they are inserted into.



## Clickable references

Articles when published often have hyperlinks associated to their title in the pdf, see e.g. [this paper](https://www.sciencedirect.com/science/article/pii/S0045782514004770). When you click on the title of a reference, it brings you to the webpage. Understanding and modifying BibTeX files are [difficult](http://tug.ctan.org/info/bibtex/tamethebeast/ttb_en.pdf). Fortunately, I came across with the [bibliography repository of Nicholas J. Higham](https://github.com/higham/njhigham-bib) in which I found the [origin](https://github.com/higham/njhigham-bib/blob/ffa8b5dffcc30979c22c5fb7e6e7dcde2656d6f1/myplain2-doi.bst#L5) of how to make hyperlinked references possible. The key idea is to modify your .bst file as detailed on http://www.math.tamu.edu/~comech/tools/bibtex-doi-eprint/ . As a quick start:

1. download the shell script from http://www.math.tamu.edu/~comech/tools/bibtex-doi-eprint/add-doi-support and save it as *add-doi-support.sh*
2. use `./add-doi-support your_bst_file.bst your_bst_file-doi.bst`
3. instead of  *your_bst_file.bst*, include the new *your_bst_file-doi.bst* in your tex file (e.g. \bibliographystyle{your_bst_file-doi})

Note that the shell script from the web page above does not need a Latex installation.



## Decreasing the file size

[pdfsizeopt](https://github.com/pts/pdfsizeopt) is a sophisticated tool to create a smaller pdf file size. Its syntax is

```bash
pdfsizeopt [OPTIONS] <tex_file> <output>
```

I used it with the flags `--do-double-check-missing-glyphs=yes` and ` --do-double-check-type1c-output=yes`. Note that depending on the number of figures, it may take a *lot* of time.



## Single source file

While keeping a modular structure of a Latex document has advantages, there are at least two situations when a single .tex file is necessary:
- journals often require the submission of a single .tex file
- some command line tools cannot follow the absolute or relative paths

`latexpand` is a command line tool shipped with the Latex distributions, which expands the included files and copy them to one file. I invoked `latexpand` as follows:

```bash
latexpand --verbose --keep-comments --empty-comments --explain --output mainSingleFile.tex main.tex
```

where the last input is my main .tex file, which includes the chapters and the other smaller parts of my thesis. The reason I used *\input* instead of *\import* from the *import* package is because `latexpand` does not work with *\import*.



## Statistics

The `texcount` command line tool counts words (or letters) and collects statistics about a .tex file. I used it with the following options:

```bash
texcount.exe [GLOBALOPTIONS] [OPTIONS] <tex_file> 
   where [GLOBALOPTIONS]: -stat -freq -macrostat -html -out=statistics.html
         [OPTIONS] could be: -v3 # coloured marked-up source included
                             -letter # count characters instead of words
```

where `<tex_file>` is the single .tex file produced by `latexpand`.



## Cropping

Removing the margins allows printing two pdf pages on a single page and makes the document legible on e-book readers. It used the `pdfcrop` command line tool with the following options:

```bash
pdfcrop --bbox-odd "65 40 510 740" --bbox-even "100 40 550 740" main.pdf main_cropped.pdf
```

Note that `pdfcrop` is relatively slow, significantly increases the size of the file, and the hyperlinks are lost (however, the pdf remains searchable). The output can be reduced with `pdfsizeopt` as described [above](#statistics).



## Miscellaneous

Some other handy tools that comes with the Latex distribution and I occasionally make use of:

- Convert a pdf to a png image
  ```bash
  pdftoppm -png input.pdf > output.png
  ```
  
- Only dump the log file with useful info
  ```bash
  texfot <compiler_such_as_pdflatex> <source.tex>
  ```
  
- Consistent indentation in the source file
  `latexindent`
  
- Mark up the difference between two .tex files
  `latexdiff`
  
  Works well with [version control](#version-control). Just get any two versions of the document as pass them to `latexdiff`.


By the way, these guides were produced with [Typora](https://typora.io/), a cross-platform markdown editor.