#!/bin/sh
version=0.5

DESCRIPTION () {
echo "
  add-doi-support: start from an existing BST file, create
  a new file with support for doi, eprint, and url fields.
  For more details, see
  http://www.math.tamu.edu/~comech/tools/bibtex-doi-eprint/

  (C) Andrew Comech, 2013.  GNU General Public License.
"
}

USAGE () {
echo "  Usage A:
  $0 [ --version | -v | --help | -h ]
         
  Usage B:
  $0 file.bst newfile.bst [-f]
  The file newfile.bst is overwritten if \`-f' is given.
"
}

LATEXCODE () {
echo "  Remember to add the following to your latex file, before \begin{document}:


\usepackage[backref=none]{hyperref} %% To make sure backref is disabled
\usepackage[usenames,dvipsnames]{color}
%% Disable backref and make \href colors more decent:
\definecolor{MyDarkBlue}{rgb}{0,0.1,0.7}
\hypersetup{pdfborder={0 0 0},colorlinks,breaklinks=true,
  urlcolor={MyDarkBlue},citecolor={MyDarkBlue},linkcolor={MyDarkBlue} }

"

}

if [ "$1" = "-v" -o "$#" = "--version" ]; then
  echo "  $0: version $version"
  echo ""
  exit
fi

if [ "$#" = "0" -o "$1" = "-h" -o "$#" = "--help" ]; then
  DESCRIPTION
  USAGE
  LATEXCODE
  exit
fi

if [ "$#" != "2" -a "$#" != "3" ]; then
  USAGE
  exit
fi

if [ "$#" = "3" -a "$3" != "-f" ]; then
  USAGE
  exit
fi

if [ ! -r "$1" ]; then
 echo "  No file $1 found. Exiting"
 USAGE
 exit
fi

if [ -r "$2" ]; then
 echo -n "  File $2 already exists... "
  if [ "$3" != "-f" ]; then  
    echo "No option \`-f' given; exiting!"
    exit
  else
    echo "Option \`-f' given; overwriting!"
  fi
fi

cp "$1" "$2"

## Do some accounting first:

sed -i "/%% This is file/s/$1/$2/" "$2"
sed -i "/%% End of file/s/$1/$2/" "$2"
sed -i "s/%% generated with the docstrip utility\./%% generated with the docstrip utility and processed with add-doi-support./" "$2"

## Add fields if not present:

[ "`grep '^    doi' "$2"`" = "" ] && sed -i '/^ENTRY/,/}/ s/}/  doi\
  }/' "$2"

[ "`grep '^    eprint' "$2"`" = "" ] && sed -i '/^ENTRY/,/}/ s/}/  eprint\
  }/' "$2"

[ "`grep '^    url' "$2"`" = "" ] && sed -i '/^ENTRY/,/}/ s/}/  url\
  }/' "$2"

## Modify the format.title (all format.*title) unless "href{" url is already present in the file:

[ "`grep 'href{..url' "$2"`" = "" ] \
&& sed -i '/^FUNCTION {format.*title}/,/^}/ s_^}_  url empty$\
    {\
    doi empty$\
      {\
      eprint empty$\
        { }\
        { "\\href{http://arxiv.org/abs/" eprint * "}{" * swap$ * "}" * }\
      if$\
      }\
      { "\\href{http://dx.doi.org/" doi * "}{" * swap$ * "}" * }\
    if$\
    }\
    { "\\href{" url * "}{" * swap$ * "}" * }\
  if$\
}_' "$2"


## If starting from NATBIB, disable extra ISSN DOI URL output:

sed -i 's/ format.url/ %format.url/;s/ format.doi/ %format.doi/;s/ format.issn/ %format.issn/' "$2"


## Add doi and eprint functions if not present:

false && [ "`grep '^FUNCTION {format.eprint}' "$2"`" = "" ] \
&& sed -i 's/^FUNCTION {article}/FUNCTION {format.eprint}\
{ eprint duplicate$ empty$ { }\
    { "\\eprint{" swap$ * "}" * }\
  if$\
}\
\
FUNCTION {article}/' "$2"

false && [ "`grep '^FUNCTION {format.doi}' "$2"`" = "" ] \
&& sed -i 's/^FUNCTION {article}/FUNCTION {format.doi}\
{ doi duplicate$ empty$ { }\
    { "\\doi{" swap$ * "}" * }\
  if$\
}\
\
FUNCTION {article}/' "$2"

# Add `format.eprint output' if not present somewhere:

false && [ "`grep 'format.eprint output' "$2"`" = "" ] \
&& sed -i '/output.bibitem/,/fin.entry/ s/fin.entry$/new.block\
  format.eprint output\
  fin.entry/' "$2"

## Add definitions of eprint if not present:

false && [ "`grep 'command.*eprint' "$2"`" = "" ] \
&& sed -i '/FUNCTION {begin.bib}/,/^}/ s_^}_  "\\providecommand{\\eprint}[1]{}\\renewcommand{\\eprint}[1]{arXiv:\\,\\href{http://arxiv.org/abs/#1}{#1}}"\
  write$ newline$\
}_' "$2"



if [ -r "$2" ]; then
  echo "  The BST file $2 has been created."
  LATEXCODE
fi
