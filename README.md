# PhD thesis

My PhD thesis and the associated material (presentation, tools, etc.)



![kuki](https://i.creativecommons.org/l/by/4.0/88x31.png) This work is licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).



## Contents

- [Binaries](#binaries)
- [Global structure](#global-structure)
- [Compiling](#compiling)
- [Questions](#questions)



## Binaries

The final version of the manuscript, the presentations and the poster can be accessed through the following links:

- [manuscript](Manuscript/main.pdf)
- presentation held at the
    - [private defense](Presentation/private/privateDefense.pdf)
    - [public seminar](Presentation/public/seminar/publicSeminar.pdf)
    - [public defense](Presentation/public/defense/publicDefense.pdf)
- [poster](Poster/BATirPoster.pdf)

If you are only interested in the pdf versions, you can stop reading here. The following is relevant for those who typeset their documents (particularly PhD student writing their theses) in $\LaTeX$ and want to get ideas about managing long, complex text.



## Global structure

The directory in which this current file resides in referred to as *ROOT* from now. The directory structure is shown by issuing the command `tree` in a Linux terminal or `tree /F` from the Windows command prompt, supposing that you are in *ROOT*. Contents of *ROOT*:

```
ROOT
|    README.md - this file
|    LICENSE - license file
|--- Bibliography - references in .bib files and their formatting
|--- Manuscript - the thesis
|--- Poster - poster prepared for the ULB
|--- Presentation - presentation of the thesis on the defense
|--- Tools - useful tools (context/spell checkers, building scripts, etc.)
|--- .git - hidden, required for version control, not important for the user
|    .gitignore - required for version control, not important for the user
```

See the *md* files of the subdirectories for further details.



## Compiling

The thesis, the presentations and the poster were typeset in $\LaTeX$. To compile them to *pdf* files, follow the steps in the corresponding README files in the directories [Manuscript](Manuscript/README.md), [Presentations](Presentation/README.md) and [Poster](Poster/README.md), respectively. The compilation was verified to work under

- MiKTeX 2.9.7140 on Windows 7
- TeX Live 2019 on Ubuntu 18.04.1



## Questions

In case of any questions (related either to the content of the thesis or to the typesetting details), [raise an issue](http://bitbucket.org/CsatiZoltan/phdthesis/issues/new).
